import sys
import pdb
import svgwrite
import json
import os
import math
import random
import Linkstream as LS
import colorGenerator as CG
from svgwrite import cm, mm, px

class idGenerator:
    """generates id"""

    def __init__(self):
        self.lookUp = dict()  # dict[Node] = id
        self.idCount = 0
        self.reverse = dict()  # dict[id] = node

    def impose(self, node, id_):
        self.lookUp[node] = id_
        self.reverse[id_] = node

    def contains(self, element):
        return element in self.lookUp

    def get(self, element):

        if element not in self.lookUp:
            while self.idCount in self.reverse and self.reverse[self.idCount] != element:
                self.idCount += 1
            self.lookUp[element] = self.idCount
            self.reverse[self.idCount] = element
        return self.lookUp[element]

    def size(self):
        return len(self.lookUp)

def evaluateOrder(L, order):
    distance = 0
    for link in L.links():
        distance += abs(order[link.left().id()]-order[link.right().id()])
    return distance

def findOrder(L, nodeID):
    cur_solution = nodeID.lookUp
    cur_reverse = nodeID.reverse
    dist = evaluateOrder(L, cur_solution)
    sys.stderr.write("Order improved from "+str(dist))
    for _ in range(0, 1000):
        i = random.randint(0, len(cur_solution) - 1)
        j = random.randint(0, len(cur_solution) - 1)
        cur_reverse[j], cur_reverse[i] = cur_reverse[i], cur_reverse[j]
        cur_solution[cur_reverse[j]] = j
        cur_solution[cur_reverse[i]] = i
        tmp = evaluateOrder(L, cur_solution)
        if tmp >= dist:
            # re swap to go back.
            cur_reverse[j], cur_reverse[i] = cur_reverse[i], cur_reverse[j]
            cur_solution[cur_reverse[j]] = j
            cur_solution[cur_reverse[i]] = i
        else:
            dist = tmp
    nodeID.lookUp = cur_solution
    new_order = "new_order.txt"
    with open(new_order, "w") as out:
        for node in nodeID.reverse:
            out.write(str(nodeID.reverse[node]) + "\n")
    sys.stderr.write(" to "+str(dist)+". Order saved in:"+new_order+"\n")


def show_help():
    print("Usage: main.py input_file [--silent] [--output=<out.svg>] [--group=aff.txt]")
    print("Input file is either a text file containing t u v d")

def read_argv(argv):
    for arg in sys.argv:
        if "=" in arg:
            content = arg.split("=")
            arg_name = content[0].replace("--", "")
            argv[arg_name] = content[1]
        elif "--" in arg:
            arg_name = arg.replace("--", "")
            argv[arg_name] = True


def version():
    sys.stderr.write("\tLinkStreamViz 1.0 -- Noe Gaumont 2016\n")


def draw(L, outputFile, Partition=False, improve=False, orderFile= False,  ppux=10):
    nodeID = idGenerator()
    for node in L.nodes():
        nodeID.get(node.id())
    if orderFile:
        tmp_nodes = set()
        with open(orderFile, 'r') as order:
            for i, n in enumerate(order):
                node = int(n)
                tmp_nodes.add(node)
                if nodeID.contains(node):
                    nodeID.impose(node, i)
                else:
                    print('The node', node, "is not present in the stream")
                    exit()
        for node in nodeID.lookUp:
            if node not in tmp_nodes:
                print('The node', node, "is not present in", orderFile)
                exit()
    if improve:
        findOrder(L, nodeID)
    ColorGen = CG.colorGenerator()
    def addDuration(G, origin, duration, color, amplitude=2):
        duration = duration * ppux
        g.add(g.line(start=(origin["x"] * px, origin["y"] * px),
                     end=( (origin["x"]+duration) * px, origin["y"] * px),
                     stroke=color,
                     stroke_opacity=0.5,
                     stroke_width=2*px))

    offset = 1.5 * ppux
    # Define dimensions
    label_margin = 5 * 2
    origleft = label_margin + 10

    right_margin = 10
    width = origleft + ppux * math.ceil(L.stop()) + right_margin
    # svgwrite._canvas_defaults["width"] = str(width) + 'px'

    arrow_of_time_height = 5
    height = 5 + 10 * int(L.sizeNode() + 1) + arrow_of_time_height
    # svgwrite._canvas_defaults["height"] = str(height) + 'px'
    g = svgwrite.Drawing(outputFile, profile='full', size=(width*px, height*px), debug=True)

    origtop = 10
    ################
    # Draw background lines
    for node in L.nodes():
        horizonta_axe = 10 * nodeID.get(node.id()) + origtop
        g.add(g.text(str(node),
                     insert=(label_margin * px, (horizonta_axe + 2) * px),
                     font_size= 6 * px,
                     style="color:black"))
        # svgwrite.mixins.Presentation().dasharray(dasharray=
        g.add(g.line(start=((origleft-5) * px,horizonta_axe * px),
                     end=((width - right_margin) * px,horizonta_axe * px),
                     stroke='grey',
                     stroke_width=1*px,
                     stroke_dasharray='2,3'))

    # Add timearrow
    g.add(g.line(start=(10 * px,10*(L.sizeNode()+1) * px),
                 end=((width-5) * px, 10*(L.sizeNode()+1) * px),
                 stroke='black',
                 stroke_width=1*px))
    g.add(g.line(start=((width-8) * px, (10*(L.sizeNode()+1)-3) * px),
                    end=((width-5) * px, 10*(L.sizeNode()+1) * px),
                    stroke='black',
                    stroke_width=1*px))
    g.add(g.line(start=((width-8) * px, (10*(L.sizeNode()+1)+3) * px),
                    end=((width-5) * px, 10*(L.sizeNode()+1) * px),
                    stroke='black',
                    stroke_width=1*px))
    g.add(g.text("Time",
                 insert=((width-19) * px, (10*(L.sizeNode()+1)+8) * px),
                 font_size= 6 * px,
                 style="color:black"))
#
# Add time ticks
    for i in range(0, int(math.ceil(L.stop())+1), 5):
        x_tick = i * ppux  + origleft
        g.add(g.line(start=(x_tick * px, (10*(L.sizeNode()+1)-3) * px),
                     end=(x_tick * px, (10*(L.sizeNode()+1)+3) * px),
                     stroke='black',
                     stroke_width=1*px))
        g.add(g.text(str(i),
                     insert=((x_tick-2) * px,(10*(L.sizeNode()+1)+9) * px),
                     font_size= 6 * px,
                     style="color:black"))

    for link in L.links():
        ts = link.start()
        node_1 = min(nodeID.get(link.left().id()), nodeID.get(link.right().id()))
        node_2 = max(nodeID.get(link.left().id()), nodeID.get(link.right().id()))
        offset = ts * ppux + origleft
        y_node1 = 10 * node_1 + origtop
        y_node2 = 10 * node_2 + origtop
        if Partition:
            gid = Partition.containing(link).id()
            color = ColorGen.get(gid)
        else:
            color = "black"
        # Add nodes
        g.add(g.circle(center=(offset * px, y_node1 * px),
                       r='1px', stroke=color,
                       stroke_width=1))
        g.add(g.circle(center=(offset * px, y_node2 * px),
                       r='1px', stroke=color,
                       stroke_width=1))


        x = 0.2 * ((10 * node_2 - 10 * node_1) / math.tan(math.pi / 3)) + offset
        y = (y_node1 + y_node2) / 2

        param_d = "M" + str(offset) + "," + str(y_node1) +\
                  " C" + str(x) + "," + str(y) + " " + str(x) + "," + str(y) +\
                  " " + str(offset) + "," + str(y_node2)
        g.add(g.path(d=param_d,
                     stroke=color,
                     fill="none"))
        if link.duration() > 0:
            addDuration(g, {"x": x, "y": (y_node1+y_node2)/2}, link.duration(), color)
# Save to svg file
    g.save()




if __name__ == '__main__':
    if len(sys.argv) < 2 or "--help" in sys.argv or "-h" in sys.argv:
        show_help()
        sys.exit()
    if "-v" in sys.argv or "--version" in sys.argv:
        version()
        exit()

    argv = {"order": "", "improve":False, "order": False, "silent": False, "group": False}
    read_argv(argv)
    L = LS.Linkstream()
    L.readFileTUV(sys.argv[1], 0)
    default_output = os.path.basename(sys.argv[1]).split(".")[0]+".svg"
    argv["output"] = argv.get("output", default_output)
    # C = LS.Partition(argv["group"],L) if argv["group"] else False
    draw(L, argv["output"],argv["group"], argv["improve"],orderFile=False, ppux=50)
    if not argv["silent"]:
        sys.stderr.write("Output generated to " + argv["output"] + ".\n")
