#ifndef LINKSTREAM_H
#define LINKSTREAM_H

#include "main.hpp"
#include "node.hpp"
#include "link.hpp"
#include "tmpLink.hpp"


#include <algorithm>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <iterator>

namespace linkstream{

class linkStream {
  friend class TestMaster;
  std::map<nodeID, Node> nodes;
  std::set<Link, ReflinkComp> links;

  std::map<Edge, std::deque<const Link *>, edgeComp> edges; // just a view of
                                                       // links in order
                                                       // to query : All
                                                       // timestamps at
                                                       // wich (a,b)
                                                       // appear.
                                                       // Order in Pair Link matter.

  mutable timestamp beginning, ending;

  linkStream(const linkStream &); // prevent copy constructor
  const linkStream &
  operator=(const linkStream &l); // prevent assignation constructor

  Node &getCreateNode(nodeID nId) {
    std::map<nodeID, Node>::iterator it = nodes.find(nId);
    if (it == nodes.end()) {
      Node &n = nodes[nId]; // creation of the node
      n.ID = nId;
      return n;
    }
    return it->second;
  }

  void pushLink(const tmpLink &l);

  bool compLinkList(const linkStream &)const;
  bool compLinkList(const std::deque<tmpLink> &)const;

  static void test(std::pair<unsigned int, unsigned int> &);
public:
  typedef std::set<Link, ReflinkComp>::iterator linksIterator;
  typedef std::set<Link, ReflinkComp>::const_iterator const_linksIterator;

  #include "linkstream_iterator.hpp"

  explicit linkStream():beginning (MAX_TIMESTAMP),ending(MIN_TIMESTAMP){}

  const_nodesIterator beginNode() const { return const_nodesIterator(nodes.begin());}
  const_nodesIterator endNode() const { return const_nodesIterator(nodes.end()); }
  nodesIterator beginNode() { return nodesIterator(nodes.begin());}
  nodesIterator endNode() { return nodesIterator(nodes.end());}
  const Node &node(nodeID nId) const;
  Node & node(nodeID nId);
  unsigned int sizeNode() const { return nodes.size(); }

  const_linksIterator beginLink() const { return links.begin(); }
  const_linksIterator endLink() const { return links.end(); }
  linksIterator beginLink() { return links.begin();}
  linksIterator endLink() { return links.end(); }
  const Link &link(linkID lId) const;

  unsigned int sizeLink() const { return links.size(); }

  const_edgesIterator beginEdge()const{return const_edgesIterator(edges.begin());}
  const_edgesIterator endEdge()const{return const_edgesIterator(edges.end());}
  edgesIterator beginEdge(){return edgesIterator(edges.begin());}
  edgesIterator endEdge(){return edgesIterator(edges.end());}
  // return all the links between a and b. Order matter.
  const std::deque<const Link *> & edge(nodeID a, nodeID b) const;
  unsigned int sizeEdge() const { return edges.size(); }



  template<SerializationFormat SF>
  void readFile(const char *, double d_duration=0);

  void addLink(const tmpLink &l);
  bool removeLink(const Link &l);
  bool removeLink(const tmpLink &l);

  void printDegreeList(std::ostream &f = std::cout) const;



  void dump() const {
    const_linksIterator it = beginLink(), ite = endLink();
    for (; it != ite; ++it) {
      const Link &l(*it);
      std::cout << "\t" << l << std::endl;
    }
  }

  // Return true if the linkstream is simple.
  bool isSimple()const;

  linkStream makeSimple()const;

  timestamp start()const{return beginning;}
  timestamp stop()const{return ending;}
  timestamp duration()const{return ending-beginning;}

};

#include "linkstream.tpp"

}
#endif
