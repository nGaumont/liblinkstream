//Class that let the user iterate on nodes.
  class const_nodesIterator{
      friend class Group;
      typedef NodeSet::const_iterator InternIterator;
      InternIterator pointer;
      const_nodesIterator(InternIterator it):pointer(it){}
      const_nodesIterator();
  public:
      const_nodesIterator(const const_nodesIterator& it): pointer(it.pointer){};
      bool operator==(const const_nodesIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const const_nodesIterator& it) const{return !(*this==it);}
      const_nodesIterator& operator++(){++pointer; return *this;} //prefix increment
      const_nodesIterator operator++(int){InternIterator tmp=pointer; ++pointer; return const_nodesIterator(tmp);} //postfix increment
      const Node& operator*() const{return **pointer;}
  };

  class const_linksIterator{
      friend class Group;
      typedef LinkSet::const_iterator InternIterator;
      InternIterator pointer;
      const_linksIterator(InternIterator it):pointer(it){}
      const_linksIterator();
  public:
      const_linksIterator(const const_linksIterator& it): pointer(it.pointer){};
      bool operator==(const const_linksIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const const_linksIterator& it) const{return !(*this==it);}
      const_linksIterator& operator++(){++pointer; return *this;} //prefix increment
      const_linksIterator operator++(int){InternIterator tmp=pointer; ++pointer; return const_linksIterator(tmp);} //postfix increment
      const Link& operator*() const{return **pointer;}
  };
