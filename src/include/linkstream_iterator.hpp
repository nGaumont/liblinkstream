//Class that let the user iterate on nodes.
  class nodesIterator{
      friend class linkStream;
      typedef std::map<nodeID, Node>::iterator InternIterator;
      InternIterator pointer;
      nodesIterator(InternIterator it):pointer(it){}
      nodesIterator();
  public:
      nodesIterator(const nodesIterator& it): pointer(it.pointer){};
      bool operator==(const nodesIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const nodesIterator& it) const{return !(*this==it);}
      nodesIterator& operator++(){++pointer; return *this;} //prefix increment
      nodesIterator operator++(int){InternIterator tmp=pointer; ++pointer; return nodesIterator(tmp);} //postfix increment
      Node& operator*() const{return pointer->second;}
  };

  class const_nodesIterator{
      friend class linkStream;
      typedef std::map<nodeID, Node>::const_iterator InternIterator;
      InternIterator pointer;
      const_nodesIterator(InternIterator it):pointer(it){}
      const_nodesIterator();
  public:
      const_nodesIterator(const const_nodesIterator& it): pointer(it.pointer){};
      bool operator==(const const_nodesIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const const_nodesIterator& it) const{return !(*this==it);}
      const_nodesIterator& operator++(){++pointer; return *this;} //prefix increment
      const_nodesIterator operator++(int){InternIterator tmp=pointer; ++pointer; return const_nodesIterator(tmp);} //postfix increment
      const Node& operator*() const{return pointer->second;}
  };


  class edgesIterator{
      friend class linkStream;
      typedef std::map<Edge, std::deque<const Link *>, edgeComp>::iterator InternIterator;
      InternIterator pointer;
      edgesIterator(InternIterator it):pointer(it){}
      edgesIterator();
  public:
      edgesIterator(const edgesIterator& it): pointer(it.pointer){};
      bool operator==(const edgesIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const edgesIterator& it) const{return !(*this==it);}
      edgesIterator& operator++(){++pointer; return *this;} //prefix increment
      edgesIterator operator++(int){InternIterator tmp=pointer; ++pointer; return edgesIterator(tmp);} //postfix increment
      std::deque<const Link *>& operator*() const{return pointer->second;}
      Edge edge()const{return pointer->first;}
  };

  class const_edgesIterator{
      friend class linkStream;
      typedef std::map<Edge, std::deque<const Link *>, edgeComp>::const_iterator InternIterator;
      InternIterator pointer;
      const_edgesIterator(InternIterator it):pointer(it){}
      const_edgesIterator();
  public:
      const_edgesIterator(const const_edgesIterator& it): pointer(it.pointer){};
      bool operator==(const const_edgesIterator& it) const{return this->pointer == it.pointer;}
      bool operator!=(const const_edgesIterator& it) const{return !(*this==it);}
      const_edgesIterator& operator++(){++pointer; return *this;} //prefix increment
      const_edgesIterator operator++(int){InternIterator tmp=pointer; ++pointer; return const_edgesIterator(tmp);} //postfix increment
      const std::deque<const Link *>& operator*() const{return pointer->second;}
      Edge edge()const{return pointer->first;}
  };
