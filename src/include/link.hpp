#ifndef LINK_H
#define LINK_H

#include "main.hpp"
#include "node.hpp"
#include "tmpLink.hpp"
#include <algorithm>
#include <deque>
#include <cmath>

namespace linkstream{
class Node;
class Link {
  friend class TestMaster;
  friend class linkStream;
  Node *leftNode;
  Node *rightNode;

  tmpLink makeTmpLink()const{
      return tmpLink(t,left().id(), right().id(), dur);
  }

  timestamp t;
  double dur;

  bool weakCompLinkNeighbor(const std::deque<tmpLink>& )const;
  bool weakCompLinkNeighbor(const std::deque<const Link *>& )const;

  static void test(std::pair<unsigned int, unsigned int> &);
public:
  //TODO should be private with a friend class.
  Link(tmpLink t, Node &leftN, Node &rightN)
      : leftNode(&leftN), rightNode(&rightN), t(t.t),
        dur(t.duration()) {}
  //TODO should be private with a friend class.
  explicit Link(const Link &l)
      : leftNode(l.leftNode), rightNode(l.rightNode), t(l.t),
        dur(l.dur) {}


  timestamp start()const{return t;}
  timestamp stop()const{return t + dur;}
  timestamp duration()const{return dur;}

  Node &left() const { return *leftNode; }

  Node &right() const { return *rightNode; }

  // True if the given nodeID is connected to the link.
  bool in(nodeID id) const {
    return (id == leftNode->id() || id == rightNode->id());
  }

  // Give the other node connected to the link
  const Node &otherNode(const Node &n) {
    if (!in(n.id())) {
      std::cerr << "The node " << n << " is not in the link " << this->leftNode
                << " " << this->rightNode << std::endl;
    }
    if (n != *leftNode) {
      return *leftNode;
    }
    return *rightNode;
  }

  // Give the degree of the link degre as the sum of the degree of each nodes (minus -2 because of the link)
  unsigned int sumDegree() const {
    return leftNode->degree() + rightNode->degree() - 2;
  }

  bool weakEqual(const Link &l) const{
      return std::fabs(t-l.t)<0.000001 && std::fabs(dur-l.duration())<0.000001 && left().id()==l.left().id() && right().id()==l.right().id();
  }
  bool weakEqual(const tmpLink &l) const{
      return std::fabs(t-l.t)<0.000001 && std::fabs(dur-l.duration())<0.000001 && left().id()==l.u && right().id()==l.v;
  }

  // Fill in the deque all the link adjacent to this
  // The deque also contains this.
  void linkNeighbor(std::deque<const Link *> &) const;

  std::pair<bool, nodeID> commonNode(const Link &l) const;

  bool sameNodes(const Link &l) const {
    return (left().id() == l.left().id() || left().id() == l.right().id()) &&
           (right().id() == l.left().id() || right().id() == l.right().id());
  }

  bool before(const Link &l) const { return t < l.t; }
  bool after(const Link &l) const { return t > l.t; }


};
inline
std::ostream &operator<<(std::ostream & os , const Link & l){
    const char *sep = " ";
    os << l.start() << sep << l.left() << sep << l.right() << sep << l.duration();
    return os;

}


struct ReflinkComp{
    bool operator()(const Link &a, const Link &b)const{
        //if a start before b then true
        if (a.start() < b.start()){
            return true;
        }
        if (a.start() > b.start()){
            return false;
        }

        if (a.left().id() < b.left().id()){
            return true;
        }
        if (a.left().id() > b.left().id()){
            return false;
        }

        if (a.right().id() < b.right().id()){
            return true;
        }
        if (a.right().id() > b.right().id()){
            return false;
        }

        if (a.duration() < b.duration()){
            return true;
        }
        if (a.duration() > b.duration()){
            return false;
        }
        return false;
    }
};

inline
bool linkComp(const Link *a, const Link *b){
    return ReflinkComp()(*a, *b);
}

struct plinkComp{
    bool operator()(const Link *a, const Link *b)const{
        return ReflinkComp()(*a, *b);
    }
};


struct plinkRefLinkComp{
    bool operator()(const Link *a, const Link &b)const{
        //if a start before b then true
        if (a->start() < b.start()){
            return true;
        }
        if (a->start() > b.start()){
            return false;
        }

        if (a->left().id() < b.left().id()){
            return true;
        }
        if (a->left().id() > b.left().id()){
            return false;
        }

        if (a->right().id() < b.right().id()){
            return true;
        }
        if (a->right().id() > b.right().id()){
            return false;
        }

        if (a->duration() < b.duration()){
            return true;
        }
        if (a->duration() > b.duration()){
            return false;
        }
        return false;
    }
};

struct pLinkTmpLinkComp{
    bool operator()(const Link *a, const tmpLink &b)const{
        //if a start before b then true
        if (a->start() < b.start()){
            return true;
        }
        if (a->start() > b.start()){
            return false;
        }

        if (a->left().id() < b.u){
            return true;
        }
        if (a->left().id() > b.u){
            return false;
        }

        if (a->right().id() < b.v){
            return true;
        }
        if (a->right().id() > b.v){
            return false;
        }

        if (a->duration() < b.duration()){
            return true;
        }
        if (a->duration() > b.duration()){
            return false;
        }
        return false;
    }
};

struct LinkTmpLinkComp{
    bool operator()(const Link &a, const tmpLink &b)const{
        //if a start before b then true
        if (a.start() < b.start()){
            return true;
        }
        if (a.start() > b.start()){
            return false;
        }

        if (a.left().id() < b.u){
            return true;
        }
        if (a.left().id() > b.u){
            return false;
        }

        if (a.right().id() < b.v){
            return true;
        }
        if (a.right().id() > b.v){
            return false;
        }

        if (a.duration() < b.duration()){
            return true;
        }
        if (a.duration() > b.duration()){
            return false;
        }
        return false;
    }
};

  typedef std::set<const Link *, plinkComp > LinkSet;
}

#endif
