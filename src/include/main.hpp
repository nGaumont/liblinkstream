#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <type_traits>
#include "traits.hpp"

#define  MAX_UINT std::numeric_limits<unsigned int>::max()
#define  MAX_TIMESTAMP std::numeric_limits<timestamp>::max()
#define  MIN_TIMESTAMP std::numeric_limits<timestamp>::lowest()

namespace linkstream {


typedef unsigned int nodeID;
typedef unsigned int linkID;
typedef unsigned int groupID;
typedef double timestamp;


typedef std::runtime_error LinkstreamException;
typedef std::pair<nodeID, nodeID> Edge;

struct edgeComp {
  bool operator()(const Edge &e1, const Edge &e2) const {
    if (e1.first > e2.first) {
      return false;
    }
    if (e1.first == e2.first) {
      return e1.second < e2.second;
    }
    return true;
  }
};

template <typename T, typename U>
inline double intersection(const T& v1,const U& v2){
    static_assert(is_interval<T>::value, "Must be an interval");
    static_assert(is_interval<U>::value, "Must be an interval");

    double dif = std::min(v1.stop(), v2.stop())- std::max(v1.start(), v2.start());
    return dif;
}


enum SerializationFormat{
    BEUV,
    TUVD,
    TUV,
    UNKNOWN
};

inline std::ostream &operator<<(std::ostream & out, const SerializationFormat & SF){
    std::string res;
    switch(SF){
        case BEUV: res="BEUV"; break;
        case TUV: res="TUV"; break;
        case TUVD: res="TUVD"; break;
        default: res="UNKNOWN"; break;
    }
    out<<res;
    return out;
}
template <typename T> static std::string toStr(const T &val) {
  std::stringstream out;
  out << val;
  return out.str();
}



} // end of namespace
#endif
