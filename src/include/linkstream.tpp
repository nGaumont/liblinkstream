
template<SerializationFormat SF>
void linkStream::readFile(const char *inputFile, double d_duration){
    this->beginning=MAX_TIMESTAMP;
    this->ending=MIN_TIMESTAMP;
  std::ifstream finput(inputFile);
  if (!finput) {
    throw LinkstreamException("The file " + toStr(inputFile) + " does not exist\n");
  }
  if(SF == SerializationFormat::TUV && d_duration<0){
     throw LinkstreamException("If your file does not have any duration then you should add a default one.");
  }

  std::string line;
  timestamp cur = MIN_TIMESTAMP;
  linkID count(0);
  while (std::getline(finput, line)) {
    tmpLink l;
    l.read<SF>(line, d_duration);
    if (l.t < cur) {
      throw LinkstreamException( "Your file is not time sorted. Line number: " + toStr(count) +"\n"+ line +"\n"+toStr(cur)+"\n");
    }
    cur = l.t;
    count++;
    pushLink(l);
  }
  finput.close();
}
