#ifndef NODE_H
#define NODE_H

#include "main.hpp"
#include "testMaster.hpp"
#include <set>

#include <deque>

namespace linkstream{

class Link;
class tmpLink;
class Node {
  friend class TestMaster;
  friend class linkStream;
  nodeID ID;
  // TODO change to ref? or const
  std::deque<const Link *> linkNeighbor;

  // add link in the right place without assuming any order between l and the neighbors
  void add(const Link &l);
  //remove link  without assuming any order between l and the neighbors
  bool remove(const Link &l);

  // l is assumed to be the newest link of the node
  void push_new(const Link &l);

  // l is assumed to be the oldest links in the neighbors
  // Return true if the removal took place
  bool pop_last(const Link &l);

  bool weakCompLinkNeighbor(const std::deque<tmpLink>& );
  bool weakCompLinkNeighbor(const std::deque<const Link *>& );

  static void test(std::pair<unsigned int, unsigned int> &);
public:
  // Should be private
  Node(nodeID id_) : ID(id_) {}
  Node() : ID(0) {}


  nodeID id() const { return ID; }

  // return a deque of link connected to the node
  const std::deque<const Link *> & linksNeighbor() const { return linkNeighbor; }

  // return the number of link connected to the node
  unsigned int degree() const {return linkNeighbor.size();}

  void displayLinkNeighbor(std::string prefix = "") const;

  bool operator==(const Node &n) const { return ID == n.id(); }
  bool operator!=(const Node &n) const { return !(n == *this); }
  bool operator<(const Node &n) const { return ID < n.id(); }
  bool operator>(const Node &n) const { return ID > n.id(); }
};

inline
std::ostream &operator<<(std::ostream & os, const Node & n){
  os << n.id();
  return os;
}

struct nodeComparator {
  bool operator()(const Node *a, const Node *b) const {
    return a->id() < b->id();
  }
};

typedef std::set<const Node *, nodeComparator > NodeSet;

}
#endif
