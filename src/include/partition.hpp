#ifndef PARTITION_H
#define PARTITION_H

#include "linkstream.hpp"
#include "group.hpp"

namespace linkstream {

class Partition {
    std::map<groupID, Group> com_struct;

    typedef std::map<const Link *, Group &, plinkComp> AssignationType;
    AssignationType assignation;

    Group & groupContaining(const Link & l);
    friend class TestMaster;
    static void test(std::pair<unsigned int, unsigned int> &);
public:

    //Class that let the user iterate on groups.
    class const_iterator{
        friend class Partition;
        typedef std::map<groupID, Group>::const_iterator InternIterator;
        InternIterator pointer;
        const_iterator(InternIterator it):pointer(it){}
        const_iterator();
    public:
        const_iterator(const const_iterator& it): pointer(it.pointer){};
        bool operator==(const const_iterator& it) const{return this->pointer == it.pointer;}
        bool operator!=(const const_iterator& it) const{return !(*this==it);}
        const_iterator& operator++(){++pointer; return *this;} //prefix increment
        const_iterator operator++(int){InternIterator tmp=pointer; ++pointer; return const_iterator(tmp);} //postfix increment
        const Group& operator*() const{return pointer->second;}
    };
    Partition::const_iterator begin() const{return const_iterator(com_struct.begin());}
    Partition::const_iterator end() const{return const_iterator(com_struct.end());}

    // Input File is affiliation file, i.e. list of link_id comID,
    // L is the linkstream that inputFile should partition.
    Partition(const char *inputFile, const linkStream & L);

    // Create a partition where each link is in its own group.
    Partition(const linkStream & L);

    // Return a const ref to the group which have a given id(panic if not existing)
    const Group & group(groupID gID)const;

    // return a ref the group which contains the link given in parameter.
    const Group & containing(const Link &)const;

    //return the number of grops in the cover.
    unsigned int size()const;

    // Check that each link has a group.
    bool checkCoverage(const linkStream & L)const;

    // // move a link to the group which has a given id.
    // //TODO change id to ref to the group.
    // void moveInto(const Link & , unsigned int);

    // write the affiliation of each link in the given ostream.
    void dumpAffiliation(std::ostream &)const;
};
inline std::ostream &operator<<(std::ostream &os, const Partition & C){
    C.dumpAffiliation(os);
    return os;
}

}// namespace end
#endif
