#ifndef PROFIL_H
#define PROFIL_H
#include "linkstream.hpp"
#include "group.hpp"
#include "main.hpp"
#include <iomanip>
#include <limits>       // std::numeric_limits
#include <cmath> // std::isnormal
#include <functional> // std::function<>
#include <cassert>
#include "partition.hpp"

#define RESOLUTION 0.000000000000001
namespace linkstream {

 double density( double duration,  double sum_deg, unsigned int nodeSize);

class Profil{
     friend class TestMaster;
     friend class DensityAnalyser;
     static void test(std::pair<unsigned int, unsigned int> &);
    //a block mean that the time interval [t;t+d] has the value val.
public:
    class Block{
        friend class TestMaster;
    public:

        Block(timestamp t_,  double d_,  double val_): t(t_), d(d_), val(val_){
            assert(d>=0);
        }
        Block(const Link & l): t(l.start()), d(l.duration()), val(1){
            assert(d>=0);
        }
        timestamp t;
        timestamp d;
        double val;

        friend std::ostream&
        operator<<(std::ostream & out, const Block &b){
            out<<"["<<b.t<<", "<<b.d<< ", "<<b.val<<"]";
            return out;
        }

        bool intersect(const Block & b)const{
            // std::cerr<<"Intersect: "<<  std::min(stop(), b.stop())- std::max(t, b.t)<< std::endl;
            return (std::min(stop(), b.stop())- std::max(t, b.t))>=0;
        }

        // Return true if this is before b.
        bool before(const Block & b)const{return (t+d)<b.t;}

        timestamp start()const{return t;}
        timestamp stop()const{return t+d;}
        timestamp duration()const{return d;}

        bool isconsistent()const{return d>0 && std::isfinite(val) && std::isfinite(d) && std::isfinite(t);}
        bool contain(timestamp t_)const{
            return ((t_>=(t)) && (t_<=(stop())));}
        bool operator==(const Block& b) const{
            return (std::abs(t-b.t)<=RESOLUTION && std::abs(d-b.d)<=RESOLUTION && std::abs(val-b.val)<=RESOLUTION);
        }
        bool operator!=(const Block& b) const{return !(*this==b);}
    };
private:
    std::map<timestamp, Block> profil;

    class iterator{
        friend class Profil;
        std::map<timestamp, Block>::iterator pointer;
        iterator(std::map<timestamp, Block>::iterator it):pointer(it){}
        iterator();

        std::map<timestamp, Block>::iterator & raw(){return pointer;}
    public:
        iterator(const iterator& it): pointer(it.pointer){};
        bool operator==(const iterator& it) const{return this->pointer == it.pointer;}
        bool operator!=(const iterator& it) const{return !(*this==it);}
        iterator& operator++(){++pointer; return *this;} //prefix increment
        iterator& operator--(){--pointer; return *this;} //prefix decrement
        Block& operator*() const{return pointer->second;}
    };

    void increment(std::map<timestamp, int>&  values, timestamp t, int incr=1, int base = 0) const {
        std::pair<std::map<timestamp, int>::iterator ,bool> ins= values.insert(std::pair<timestamp,int>(t, base+incr));
        // true if a new element was inserted or false if an equivalent key already existed.
        if (!ins.second){
            (*ins.first).second+= incr;
        }
    }
    void decrement(std::map<timestamp, int>& values, timestamp t, int incr=1, int base = 0)const {increment(values, t, -incr, base);}

    // Compute all the changement from the links inside [beg;end[ and add it to changes (change being a sketch of degree profil)
    // This functions consider that all the links are between the same 2 nodes.
    // This function is usefull if the linkstream is not simple.
    void changementFrom(std::deque<const Link *>::const_iterator beg, std::deque<const Link *>::const_iterator end, std::map<timestamp, int> & changes)const;
    Profil::iterator begin(){return iterator(profil.begin());}
    Profil::iterator end(){return iterator(profil.end());}

    // Try to insert the bloc in the profil by adding value to existing block or by spliting.
    // Return an irator to a block in profil shuch that it follow the inserted block;
    Profil::iterator add_hint(Profil::iterator, Block);

    // Return an irator to a block in profil shuch that it follow the inserted block;
    Profil::iterator modifier_hint(Profil::iterator, Block, std::function<double (const Block &, const Block &)> func);


    Profil::iterator at(timestamp t, bool before =true){
        std::map<timestamp, Block>::iterator tmp = profil.lower_bound(t);
        if( tmp != profil.begin()){
            --tmp;
        }
        if(!before  && std::abs(t-(*tmp).second.stop())<RESOLUTION){
            ++tmp;
            if(tmp == profil.end()){
                --tmp;
            }
        }
        return iterator(tmp);
    }
    explicit Profil():profil(){}
    explicit Profil(const std::deque<Link*>&);

    std::map<timestamp, int> activations(const linkStream & L, const std::set<nodeID> & node)const;
    std::map<timestamp, int> extended_activations(const linkStream & L, const std::set<nodeID> & node)const;
    std::map<timestamp, int> activations_notSimple(const linkStream & L, const std::set<nodeID> & node)const;
    std::map<timestamp, int> extended_activations_notSimple(const linkStream & L, const std::set<nodeID> & node)const;

    std::map<timestamp, int> activations(const Group & g)const;
    std::map<timestamp, int> inter_activations(const linkStream & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const;
    std::map<timestamp, int> activations_notSimple(const Group & g)const;
    std::map<timestamp, int> inter_activations_notSimple(const linkStream & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const;

    void fillProfil(const std::map<timestamp, int> & changes, bool clear_profil=true);

public:
    explicit Profil(timestamp beg, timestamp end, double val=0);
    explicit Profil(const std::map<timestamp, Block>& p_): profil(p_){}
    Profil(const Profil & p):profil(p.profil){}

    // Build the profil when considerings links between nodes only (incluse == true) or considering links having at least one node in nodes (incluse == false);
    explicit Profil(const linkStream & L, const NodeSet & nodes, bool incluse, bool assume_simple);
    explicit Profil(const linkStream & L, const std::set<nodeID> & nodes, bool incluse , bool assume_simple);
    explicit Profil(const Group & g, bool assume_simple=true);
    explicit Profil(const linkStream &, bool assume_simple=true);

    //Compute the degree profil of the linkstream if a delta is considered.
    explicit Profil(const linkStream & L, double delta);
    explicit Profil(const Group & g, double delta);
    //incluse == true then only links between "nodes" are considered
    //incluse == false then all links having at least one node in "nodes" are considered.
    explicit Profil(const linkStream & L, const std::set<nodeID> & nodes, double delta, bool incluse);

    static Profil inter(const linkStream& L, const Group & g1, const Group & g2, const std::set<nodeID> & union_nodes, bool assume_simple=true){
        Profil p;
        std::map<timestamp, int> changes;
        if(assume_simple){
            changes = p.inter_activations(L, g1,g2, union_nodes);
        }else{
            changes = p.inter_activations_notSimple(L, g1,g2, union_nodes);
        }
        p.fillProfil(changes);
        return p;
    }

    class const_iterator{
        friend class Profil;
        std::map<timestamp, Block>::const_iterator pointer;
        const_iterator(std::map<timestamp, Block>::const_iterator it):pointer(it){}
        const_iterator();
        const_iterator(const iterator & it):pointer(it.pointer){};
    public:
        const_iterator(const const_iterator& it): pointer(it.pointer){};
        bool operator==(const const_iterator& it) const{return this->pointer == it.pointer;}
        bool operator!=(const const_iterator& it) const{return !(*this==it);}
        const_iterator& operator++(){++pointer; return *this;} //prefix increment
        const_iterator operator++(int){auto tmp=pointer; ++pointer;return const_iterator(tmp);} //postfix increment
        const_iterator& operator--(){--pointer; return *this;} //prefix decrement
        const Block& operator*() const{return pointer->second;}
    };
    Profil::const_iterator begin() const{return const_iterator(profil.begin());}
    Profil::const_iterator end() const{return const_iterator(profil.end());}

    Profil::const_iterator cbegin() const{return const_iterator(profil.begin());}
    Profil::const_iterator cend() const{return const_iterator(profil.end());}

    //Compare two profile;
    bool operator==(const Profil &) const;


    //Return an iterator to the block b, so that t \in ]block.begin; block.end]
    // If before is false then th interval is  [block.begin; block.end[
    // If t is not inside the profile return the closest one.
    Profil::const_iterator at(timestamp t, bool before =true)const{
        return const_cast<Profil &>(*this).at(t,before);
    }

    // Compute the addition of two profil.
    void add(const Profil & p);


    // Mix *this with the profil "p" according to the func_pointer.
    // It changes *this only on the intersection between *this and p.
    void modifier(const Profil & p, std::function<double (const Block &, const Block &)> func);
    void modifier(std::function<double (const Block &)> func);

    // Compute the  area under the curve (Sum of val*d for each block inside [beg,end]);
    double AUC(timestamp, timestamp)const;

    // Return the size of the profil
    unsigned int size()const{return profil.size();}

    //return an iterator to the block having the max value
    Profil::const_iterator maxVal()const{
        Profil::const_iterator res=begin();
        for(auto it = begin(); it!=end();++it){
            if((*it).val>(*res).val){
                res = it;
            }
        }
        return res;
    }

    // Consider the profil to represent a stepfunction and compute the time proportion when the profil is under the threshold
    double percentil_stepFunction(double threshold)const;

    // Consider the profil to represent a pointwise function and compute the time proportion when the profil is under the threshold
    double percentil_pointwise(double threshold)const;

    timestamp start()const{return profil.begin()->second.t;}
    timestamp stop()const{return profil.rbegin()->second.stop();}
    timestamp duration()const {return stop()-start();}

    bool isconsistent()const;

    void dump()const;
};
    std::ostream& operator<<(std::ostream & out, const Profil & p);
}
#endif
