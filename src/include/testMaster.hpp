#ifndef TESTMASTER_H
#define TESTMASTER_H

#include <iostream>


namespace linkstream {

inline
std::string singleTest(bool b, std::pair<unsigned int, unsigned int> & res){
    ++res.second;
    if(!b){
        ++res.first;
        return "fail!";
    }
    return "ok.";
}

struct TestMaster{
    bool all()const;
};
}
#endif
