#ifndef GROUP_H
#define GROUP_H

#include "main.hpp"
#include "link.hpp"
#include "node.hpp"
#include "linkstream.hpp"

namespace linkstream {

class Group {
  LinkSet link_set;
  mutable NodeSet node_set;
  mutable bool updateNodeset;
  groupID identifier;


  mutable timestamp beginning,ending;
  mutable bool updateTimeSpan;
  bool add_hint(LinkSet::iterator hint, const Link & l);

  void updateTime()const;
  friend class Partition;
  friend class TestMaster;
  static void test(std::pair<unsigned int, unsigned int> &);
 public:

     #include "group_iterator.hpp"

    //TODO make a two comparator: adresse comparator and content comparator.
    bool operator==(const Group & g)const{return this == &g;}
    Group():link_set(), node_set(), updateNodeset(false), identifier(),beginning(MAX_TIMESTAMP),ending(MIN_TIMESTAMP),updateTimeSpan(false){}

    //Return the id of the group.
    groupID id()const{return identifier;}

    // return True if the link has been added to the group
    bool add(const Link & l);
    // return True if the link has been remove from the group
    // The group might be empty after this.
    bool remove(const Link & l);

    // give a view on the set of nodes which are at least connected to on link of the group.
    const NodeSet &  inducedNode()const;

    // return True if the link given in parameter is in the group.
    bool contains(const Link & )const;

    const_nodesIterator beginNode() const {inducedNode(); return const_nodesIterator(node_set.begin());}
    const_nodesIterator endNode() const {inducedNode(); return const_nodesIterator(node_set.end()); }
    unsigned int sizeNode() const {inducedNode(); return node_set.size(); }

    const_linksIterator beginLink() const { return link_set.begin(); }
    const_linksIterator endLink() const { return link_set.end(); }
    unsigned int sizeLink() const { return link_set.size(); }


    timestamp start()const{return (*link_set.begin())->start();}
    timestamp stop()const{if(!updateTimeSpan) updateTime();return ending;}
    timestamp duration()const{return stop()-start();}


    void dump()const{
        for(auto link: link_set){
            std::cout<<*link<<std::endl;
        }
    }
};
inline
std::ostream &operator<<(std::ostream &out, const Group &g){
    out<<"Group :"<<g.id() <<std::endl;
    auto it = g.beginLink(), ite=g.endLink();
    for(;it!=ite;++it) {
        out<<*it<<", ";
    }
    return out;
}

}

#endif
