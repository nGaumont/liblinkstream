#ifndef TMPLINK_H
#define TMPLINK_H

#include "main.hpp"
#include <string>
namespace linkstream {
class Link;
class tmpLink {
    friend class linkStream;
    friend class Link;

    template <SerializationFormat SF>
    void read(const std::string&, double=0);

public:
  tmpLink(): u(MAX_UINT), v(MAX_UINT), t(-1), dur(-1) {}
  tmpLink(const tmpLink &l): u(l.u), v(l.v), t(l.t), dur(l.dur) {}
  tmpLink(timestamp t_, nodeID u_, nodeID v_, timestamp dura_)
          : u(u_), v(v_), t(t_), dur(dura_) {
              if (dur < 0) {
                std::string s("tmpLink: The duration should be positive"+toStr(dur)+"\n");
                throw LinkstreamException(s);
              }
    }
  tmpLink(timestamp t_, timestamp end_, nodeID u_, nodeID v_)
          : u(u_), v(v_), t(t_), dur(end_-t_) {
              if (dur < 0) {
                std::string s("tmpLink: The end should be after the begining "+ toStr(t_) +" "+toStr(end_)+"\n");
                throw LinkstreamException(s);
              }
    }

    void operator=(const Link &);

  bool empty()const{return u==MAX_UINT && v==MAX_UINT && t==-1 && dur==-1;}

  timestamp start()const{return t;}
  timestamp stop()const{return t+dur;}
  timestamp duration()const{return dur;}

  nodeID u;
  nodeID v;
  timestamp t;
  double dur;
};


template <>
inline void tmpLink::read<SerializationFormat::TUVD>(const std::string& line, double add_dur){
    if (add_dur <0){
        std::cerr << "The duration to add can't be negative :" << add_dur << std::endl;
    }
    std::istringstream iss(line);
    bool readed =false;
    readed = static_cast<bool>(iss >> t >> u >> v >> dur);
    if (!(readed) || dur < 0) {
      std::string s("There is a problem with the line :\n" + line+"\n");
      throw LinkstreamException(s);
    }
    t = t-add_dur/2;
    dur+= add_dur/2;
}

template <>
inline void tmpLink::read<SerializationFormat::TUV>(const std::string& line, double add_dur){
    if (add_dur <0){
        std::cerr << "The duration to add can't be negative :" << add_dur << std::endl;
    }
    std::istringstream iss(line);
    bool readed =false;
    readed = static_cast<bool>(iss >> t >> u >> v);
    dur = add_dur;
    if (!(readed) || dur < 0) {
      std::string s("There is a problem with the line :\n" + line+"\n");
      throw LinkstreamException(s);
    }
    t = t-add_dur/2;
}

template <>
inline void tmpLink::read<SerializationFormat::BEUV>(const std::string& line, double add_dur){
    if (add_dur <0){
        throw LinkstreamException("The duration to add can't be negative :" + toStr(add_dur) + "\n");
    }
    std::istringstream iss(line);
    bool readed =false;
    timestamp t_end;
    long double duration;
    readed = static_cast<bool>(iss >> t >> t_end >> u >> v);
    duration = t_end - t;
    if (!(readed) || duration < 0) {
        std::string s("There is a problem with the line :\n" + line+"\n");
        throw LinkstreamException(s);
    }
    dur = duration;
    t=t-add_dur/2;
    dur+= add_dur/2;
}

template <SerializationFormat SF>
inline void tmpLink::read(const std::string& line, double d){
    std::string s ("read() not defined for  "+ toStr(SF)+"\n Parameter were "+line+"  "+toStr(d)+"\n");
    throw LinkstreamException(s);
}

inline std::ostream &operator<<(std::ostream & out, const tmpLink & l){
    out<<l.t<<" "<<l.u<<" "<<l.v<<" "<<l.dur;
    return out;
}

}

#endif
