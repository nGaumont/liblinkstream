#ifndef TRAIT_H
#define TRAIT_H

namespace linkstream {

// http://stackoverflow.com/questions/874298/c-templates-that-accept-only-certain-types
// http://pfultz2.com/blog/2014/08/17/type-requirements/
template<typename...>
struct void_ {
    using type = void;
};

template<typename... Args>
using Void = typename void_<Args...>::type;

struct is_interval_impl {
    template<typename T, typename Begin = decltype(std::declval<const T&>().start()),
                         typename End   = decltype(std::declval<const T&>().stop()),
                         typename Duration   = decltype(std::declval<const T&>().duration())>
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};
template<typename T>
struct is_interval : decltype(is_interval_impl::test<T>(0)) {};

struct is_nodeIterable_impl {
    template<typename T, typename BeginNode = decltype(std::declval<const T&>().beginNode()),
                         typename EndNode   = decltype(std::declval<const T&>().endNode()),
                         typename sizeNode   = decltype(std::declval<const T&>().sizeNode())>
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};
template<typename T>
struct is_nodeIterable : decltype(is_nodeIterable_impl::test<T>(0)) {};

struct is_linkIterable_impl {
    template<typename T, typename BeginLink = decltype(std::declval<const T&>().beginLink()),
                         typename EndLink   = decltype(std::declval<const T&>().endLink()),
                         typename sizeLink   = decltype(std::declval<const T&>().sizeLink())>
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};
template<typename T>
struct is_linkIterable : decltype(is_linkIterable_impl::test<T>(0)) {};


struct is_edgeIterable_impl {
    template<typename T, typename BeginEdge = decltype(std::declval<const T&>().beginEdge()),
                         typename EndEdge   = decltype(std::declval<const T&>().endEdge()),
                         typename sizeEdge   = decltype(std::declval<const T&>().sizeEdge())>
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};
template<typename T>
struct is_edgeIterable : decltype(is_edgeIterable_impl::test<T>(0)) {};

} //end of name space
#endif
