#include "interface.hpp"
#include "partition.hpp"

namespace py = pybind11;
namespace linkstream{

void Partition_export(py::module &m){
    // Partition
    py::class_<Partition> partition(m, "Partition",R"doc(Partition)doc");
    partition.def(py::init<const linkStream &>(),"Contructor where each link is in its own group.",py::return_value_policy::take_ownership);
    partition.def(py::init<const char *, const linkStream &>(),"Contructor which reads an affilation list (lid groupid).",py::return_value_policy::take_ownership);
    partition.def("containing", &Partition::containing, "Return the group which contains the link.", py::return_value_policy::reference_internal);
    partition.def("group", &Partition::group, "Return the group in the partition having its id equal id.", py::return_value_policy::reference_internal);
    partition.def("size", &Partition::size, "Return the number of groups in the partition.");
    partition.def("checkCoverage", &Partition::checkCoverage,"Check that each link is in a group.");
    partition.def("__repr__",[](const Partition &P) {
            return toStr(P);
        }
    );

    pybind11::class_<PyIterator<Partition::const_iterator>>(partition, "GroupsIterator")
     .def("__iter__", [](PyIterator<Partition::const_iterator> &it) -> PyIterator<Partition::const_iterator>& { return it; })
     .def("__len__", &PyIterator<Partition::const_iterator>::size)
     .def("__next__", &PyIterator<Partition::const_iterator>::next,"Return the group and advance the pointer.", pybind11::return_value_policy::reference_internal);

    partition.def("groups", [](py::object s) {
            Partition & P =s.cast<Partition &>();
            return PyIterator<Partition::const_iterator>(P.begin(),P.end(), P.size(), s);
        },
        "Return an interator over the groups in the Partition."
    );

}

} // end of namspace
