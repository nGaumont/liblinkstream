#include "interface.hpp"
#include "linkstream.hpp"

namespace py = pybind11;
namespace linkstream{

struct PypActivationIterator{
    PypActivationIterator(const std::deque<const Link *> & deq, Edge e_,  unsigned int size_, py::object ref) : len(size_), e(e_) , it(deq.begin()), last(deq.end()), ref(ref) { }
    PypActivationIterator(std::deque<const Link *>::const_iterator beg, std::deque<const Link *>::const_iterator end, Edge e_, unsigned int size_, py::object ref) : len(size_), e(e_), it(beg), last(end), ref(ref) { }

    unsigned int len;
    Edge e;
    Edge edge(){return e;}
    unsigned int size()const{return len;}
    const Link * next() {
        if (it == last)
            throw py::stop_iteration();
        return *(it++);
    }

    typename std::deque<const Link *>::const_iterator it;
    typename std::deque<const Link *>::const_iterator last;
    py::object ref; // keep a reference
};

struct PyEdgesIterator {
    PyEdgesIterator(linkStream::edgesIterator beg, linkStream::edgesIterator last,unsigned int size_, py::object ref) : len(size_), it(beg), last(last), ref(ref) { }

    unsigned int len;
    unsigned int size()const{return len;}
    PypActivationIterator next() {
        if (it == last)
            throw py::stop_iteration();
        linkStream::edgesIterator prev = it;
        ++it;
        return PypActivationIterator(*prev, prev.edge(), (*prev).size(), ref);
    }



    linkStream::edgesIterator it;
    linkStream::edgesIterator last;
    py::object ref; // keep a reference
};

void Linkstream_export(py::module &m){

    py::class_<linkStream> L(m, "Linkstream",R"doc(Linkstream)doc");

    pybind11::class_<PyIterator<linkStream::linksIterator>>(L, "LinksIterator")
     .def("__iter__", [](PyIterator<linkStream::linksIterator> &it) -> PyIterator< linkStream::linksIterator>& { return it; })
     .def("__len__", &PyIterator<linkStream::linksIterator>::size)
     .def("__next__", &PyIterator<linkStream::linksIterator>::next,"Return the link and advance the pointer.", pybind11::return_value_policy::reference_internal);

     py::class_<PypActivationIterator>(L, "ActivationsIterator")
     .def("__iter__", [](PypActivationIterator &it) -> PypActivationIterator& { return it; })
     .def("__len__", &PypActivationIterator::size)
     .def("edge", &PypActivationIterator::edge)
     .def("__next__", &PypActivationIterator::next, "Return the link corresponding to the next activation and advance the pointer.", py::return_value_policy::reference_internal);


    pybind11::class_<PyIterator<linkStream::nodesIterator> >(L, "NodesIterator")
     .def("__iter__", [](PyIterator<linkStream::nodesIterator> &it) -> PyIterator<linkStream::nodesIterator>& { return it; })
     .def("__len__", &PyIterator<linkStream::nodesIterator>::size)
     .def("__next__", &PyIterator<linkStream::nodesIterator>::next,"Return the node and advance the pointer.", pybind11::return_value_policy::reference_internal);

     py::class_<PyEdgesIterator>(L, "EdgeIterator")
     .def("__iter__", [](PyEdgesIterator &it) -> PyEdgesIterator& { return it; })
     .def("__len__", &PyEdgesIterator::size)
     .def("__next__", &PyEdgesIterator::next,"Return the edge and advance the pointer.", py::return_value_policy::copy);



    L.def(py::init(),"Constructor", py::return_value_policy::take_ownership);
    L.def("readFileBEUV",  &linkStream::readFile<BEUV>,
        "Read the file passed in argument as a list of links in the format BEUV as argument.",
        py::arg("inputFile"),
        py::arg("d_duration")=0
    );
    L.def("readFileTUVD",  &linkStream::readFile<TUVD>,
        "Read the file passed in argument as a list of links in the format TUVD as argument.",
        py::arg("inputFile"),
        py::arg("d_duration")=0
    );
    L.def("readFileTUV",  &linkStream::readFile<TUV>,
        "Read the file passed in argument as a list of links in the format TUV as argument and add as duration the default duration."
    );
    L.def("addLink",  &linkStream::addLink,
        "Add a link to the link stream without assuming time information."
    );
    L.def("removeLink", (bool (linkStream::*)(const Link&)) &linkStream::removeLink,
        "Remove a link from the link stream without assuming time information."
    );
    L.def("removeLink", (bool (linkStream::*)(const tmpLink &)) &linkStream::removeLink,
        "Remove a link from the link stream without assuming time information."
    );
    L.def("isSimple", &linkStream::isSimple, "Return if the stream is simple.");


    L.def("links", [](py::object s) {
            linkStream & L =s.cast<linkStream &>();
            return PyIterator<linkStream::linksIterator>(L.beginLink(), L.endLink(), L.sizeLink(), s);
        },
        "Return an interator over the links in the link stream."
    );
    L.def("link", (Link & (linkStream::*)(linkID)) &linkStream::link, "Return the link which has a given ID.", py::return_value_policy::reference_internal);
    L.def("start",  &linkStream::start, "Return the begining of the link stream.");
    L.def("stop",  &linkStream::stop, "Return the ending of the link stream.");
    L.def("duration",  &linkStream::duration, "Return the duration of the link stream.");

    L.def("sizeNode", &linkStream::sizeNode, "Return the number of nodes.");
    L.def("sizeLink", &linkStream::sizeLink, "Return the number of links.");
    L.def("node", (Node & (linkStream::*)(nodeID)) &linkStream::node, "Return the node which has a given ID.", py::return_value_policy::reference_internal);
    L.def("nodes", [](py::object s) {
            linkStream & L =s.cast<linkStream &>();
            return PyIterator<linkStream::nodesIterator>(L.beginNode(), L.endNode(), L.sizeNode(),  s);
        },
        "Return an interator over the nodes in the Link stream."
    );

    L.def("edge", [](py::object s, nodeID a, nodeID b) {
            linkStream & L =s.cast< linkStream &>();
            const std::deque<const Link *> & activations = L.edge(a,b);
            return PypActivationIterator(activations.begin(), activations.end(), Edge(a,b), activations.size(), s);
        },
        "Return an interator over the activations of the node pair (a,b) in the link stream."
    );
    L.def("edges", [](py::object s) {
            linkStream & L =s.cast<linkStream &>();
            return PyEdgesIterator(L.beginEdge(), L.endEdge(), L.sizeEdge(),  s);
        },
        "Iterator over the edges in the link stream."
    );
    L.def("__repr__", [](const linkStream &L) {
            return "<class Linkstream "+toStr(&L)+">";
        }
    );
}

}
