#include "interface.hpp"
#include "profil.hpp"

namespace py = pybind11;
namespace linkstream{

void Profil_export(py::module &m){

    py::class_<Profil> profil(m, "Profil",R"doc(Profil)doc");
    pybind11::class_<PyIterator<Profil::const_iterator>>(profil, "BlocksIterator")
     .def("__iter__", [](PyIterator<Profil::const_iterator> &it) -> PyIterator<Profil::const_iterator>& { return it; })
     .def("__len__", &PyIterator<Profil::const_iterator>::size)
     .def("__next__", &PyIterator<Profil::const_iterator>::next,"Return the block and advance the pointer.", pybind11::return_value_policy::reference_internal);

    profil.def(py::init<const Group &, bool>(),"Construct the profil when taking only the links in the group into account. The boolean indicates if the stream is simple or not.", py::return_value_policy::take_ownership);
    profil.def(py::init<const linkStream &, bool>(),"Construct the profil when taking the whole DurationStream into account. The boolean indicates if the stream is simple or not.", py::return_value_policy::take_ownership);
    profil.def(py::init<const Profil &>(),"Copy Constructor", py::return_value_policy::take_ownership);
    profil.def(py::self == py::self);
    profil.def("__repr__",[](const Profil &P) {
            return toStr(P);
        }
    );

    profil.def("blocks", [](py::object s) {
            const Profil & P =s.cast<const Profil &>();
            return PyIterator<Profil::const_iterator>(P.begin(), P.end(), P.size(), s);
        },
        "Return an interator over the block in the profil"
    );
    profil.def("add", &Profil::add,"Add the profil passed in parameter into the profil");
    profil.def("isConsistent", &Profil::isconsistent,"Test if the profil is consistent (Continous time)");
    profil.def("at", [](const Profil & P, timestamp t, bool before) -> const Profil::Block &{
            const Profil::Block & b = *P.at(t, before);
            return b;
        },
        "Return the block containing the timestamp t. The boolean changes block interval either as ]b;e] (if true) or as [b;e[ (if false)."
        ,py::return_value_policy::reference_internal
    );
    profil.def("AUC", &Profil::AUC,"Compuute the AUC of the profil in the interval [begin: end]."
    );
    profil.def("size", &Profil::size,"Return the number of block in the profil");
    profil.def("maxVal", [](const Profil & P) -> const Profil::Block &{
            return *P.maxVal();
        },
        "Return the block containing the maximum value in the profil.",
        py::return_value_policy::reference_internal
    );
    profil.def("percentil_stepFunction", &Profil::percentil_stepFunction,
        "Compute the time proportion under threshold when the profil is a step function."
    );
    profil.def("percentil_pointwise", &Profil::percentil_pointwise,
        "Compute the time proportion under threshold when the profil is a piecewise linear function."
    );

    py::class_<Profil::Block> block(profil, "Block");
    block.def("__repr__",[](const Profil::Block &b) {
            return toStr(b);
        }
    );
   block.def("start", &Profil::Block::start, "Return the begining of the Block.");
   block.def("stop", &Profil::Block::stop, "Return the ending of the Block.");
   block.def("duration", &Profil::Block::duration, "Return the ending of the Block.");
   block.def_readwrite("val", &Profil::Block::val,"The value at the begining of block (and during the whole time span if the profil is piece wise constant).");
}


} //end of namspace
