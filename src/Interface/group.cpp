#include "interface.hpp"
#include "group.hpp"

namespace py = pybind11;
namespace linkstream{

void Group_export(py::module &m){
    // Group
    py::class_<Group> group(m, "Group",R"doc(Group)doc");
    pybind11::class_<PyIterator<Group::const_linksIterator>>(group, "LinksIterator")
     .def("__iter__", [](PyIterator<Group::const_linksIterator> &it) -> PyIterator<Group::const_linksIterator>& { return it; })
     .def("__len__", &PyIterator<Group::const_linksIterator>::size)
     .def("__next__", &PyIterator<Group::const_linksIterator>::next,"Return the link and advance the pointer.", pybind11::return_value_policy::reference_internal);


    pybind11::class_<PyIterator<Group::const_nodesIterator>>(group, "NodesIterator")
     .def("__iter__", [](PyIterator<Group::const_nodesIterator> &it) -> PyIterator<Group::const_nodesIterator>& { return it; })
     .def("__len__", &PyIterator<Group::const_nodesIterator>::size)
     .def("__next__", &PyIterator<Group::const_nodesIterator>::next,"Return the node and advance the pointer.", pybind11::return_value_policy::reference_internal);


    group.def("id", &Group::id,"Return the id of the group");
    group.def("add", &Group::add, "Add a link to the group. Return true if the link is inserted and false if the link was already in the set.");
    group.def("remove", &Group::remove, "Remove a link from the group. Return true if a link has been removed.");
    group.def("inducedNodes", [](py::object s){
            const Group & g= s.cast<const Group &>();
            return PyIterator<Group::const_nodesIterator>(g.beginNode(), g.endNode(), g.sizeNode(), s);
        }, "Return an iterator over the nodes induced by the group link."
    );
    group.def("nodes", [](py::object s) {
            const Group & g =s.cast<Group &>();
            return PyIterator<Group::const_nodesIterator>(g.beginNode(), g.endNode(), g.sizeNode(), s);
        },
        "Return an interator over the induced nodes in the groups."
    );
    group.def("sizeNode", &Group::sizeNode, "Return the number of induced nodes by the group.");

    group.def("links", [](py::object s) {
            Group & g =s.cast<Group &>();
            return PyIterator<Group::const_linksIterator>(g.beginLink(), g.endLink(), g.sizeLink(), s);
        },
        "Return an interator over the links in the groups."
    );
    group.def("sizeLink", &Group::sizeLink, "Return the number of links in thr group.");

    group.def("start",  &Group::start, "Return the begining of the group.");
    group.def("stop",  &Group::stop, "Return the ending of the group.");
    group.def("duration",  &Group::duration, "Return the duration of the group.");

    group.def("contains", &Group::contains, "Return true if the group contains the link.");
    group.def("__repr__",[](const Group &g) {
            return "<class Group "+toStr(&g)+">";
        }
    );
}

} // end of namspace
