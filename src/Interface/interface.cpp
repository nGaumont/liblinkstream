#include "interface.hpp"
#include "testMaster.hpp"
using namespace linkstream;

PYBIND11_PLUGIN(Linkstream) {
    py::module m("Linkstream", R"doc(Linkstream)doc");

    m.def("test", []() -> bool {
        TestMaster test;
        char temp[300];
        getcwd(temp, 300);
        std::cout<<"Testing from "<< std::string(temp)<<std::endl;

        return test.all();
    }, R"pbdoc(
        Test that evrything works fine.
    )pbdoc");

    Node_export(m);
    tmpLink_export(m);
    Link_export(m);
    Linkstream_export(m);
    Group_export(m);
    Partition_export(m);
    Profil_export(m);
    DensityAnalyser_export(m);

    return m.ptr();
}
