#include <pybind11/pybind11.h>
#include "pybind11/stl.h"
#include "pybind11/operators.h" // used for py::self

namespace py = pybind11;

namespace linkstream{

template<typename T, typename V= decltype(*(std::declval<T>())) >
class PyIterator {
    unsigned int len;
public:
    PyIterator(T beg, T last, unsigned int size, pybind11::object ref) : len(size), it(beg), end(last), ref(ref) { }

    V next() {
        if (it == end)
            throw pybind11::stop_iteration();
        return *(it++);
    }

    unsigned int size() const {return len;}

    T it;
    T end;
    pybind11::object ref; // keep a reference
};
void tmpLink_export(py::module &);
void Node_export(py::module &);
void Link_export(py::module &);
void Linkstream_export(py::module &);
void Group_export(py::module &);
void Partition_export(py::module &);
void Profil_export(py::module &);
void DensityAnalyser_export(py::module &);

}
