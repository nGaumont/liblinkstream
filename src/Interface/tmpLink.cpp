
#include "tmpLink.hpp"
#include "interface.hpp"
namespace py = pybind11;

namespace linkstream{

void tmpLink_export(py::module &m){
    py::class_<tmpLink>(m, "tmpLink",R"doc(tmpLink)doc")
    .def(py::init<timestamp, nodeID, nodeID, timestamp>(),"create a temporary link to add in a link stream. Expected format TUVD")
    .def(py::init<timestamp, timestamp, nodeID, nodeID>(),"create a temporary link to add in a link stream. Expected format BEUV")
    .def_readwrite("t", &tmpLink::t)
    .def_readwrite("u", &tmpLink::u)
    .def_readwrite("v", &tmpLink::v)
    .def_readwrite("dur", &tmpLink::dur)
    .def("__repr__", [](const tmpLink &l) {
            return toStr(l);
        }
    );

}

}
