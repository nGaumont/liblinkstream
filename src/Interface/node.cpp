#include "interface.hpp"
#include "link.hpp"

namespace py = pybind11;
namespace linkstream{

void Node_export(py::module &m){
    py::class_<Node> node(m,"Node",R"doc(Node)doc");

    node.def("id", &Node::id, R"doc(
        Return the id of the node.
    )doc");
    node.def("links", [](pybind11::object s) {
            Node & n = s.cast<Node &>();
            return PyIterator<std::deque<const Link *>::const_iterator>(n.linksNeighbor().cbegin(),n.linksNeighbor().cend(),n.linksNeighbor().size(), s);
        },
        R"doc(
            Return an iterator over the links connected to the node.
        )doc"
    );
    node.def("degree", &Node::degree, R"doc(
        Return the number of links connected to the node
    )doc");
    node.def("sizeLinks", &Node::degree, R"doc(
        Return the number of links connected to the node
    )doc");
    node.def(pybind11::self == pybind11::self);
    node.def(pybind11::self != pybind11::self);
    node.def("__repr__", [](const Node &n) {
            return toStr(n);
        }
    );
    pybind11::class_<PyIterator<std::deque<const Link *>::const_iterator> >(m, "LinksIterator")
     .def("__iter__", [](PyIterator<std::deque<const Link *>::const_iterator> &it) -> PyIterator<std::deque<const Link *>::const_iterator>& { return it; })
     .def("__len__", &PyIterator<std::deque<const Link *>::const_iterator>::size)
     .def("__next__", &PyIterator<std::deque<const Link *>::const_iterator>::next,"Return the link and advance the pointer.", pybind11::return_value_policy::reference_internal);
}
}
