#include "interface.hpp"
#include "densityAnalyser.hpp"

namespace py = pybind11;
namespace linkstream{

void DensityAnalyser_export(py::module &m){
    // DensityAnalyser
    py::class_<DensityAnalyser> Dens(m, "DensityAnalyser",R"doc(DensityAnalyser)doc");
    Dens.def(py::init<const linkStream &, bool>(),"Analyser for a whole link stream (bool denotes if group is simple).", py::return_value_policy::take_ownership);
    Dens.def(py::init<const Group &, bool>(),"Analyser for a single group (bool denotes if group is simple).", py::return_value_policy::take_ownership);
    Dens.def(py::init<const linkStream &,const std::vector<nodeID> &, bool , bool>(), "Analyser for a set of node V' bool: true consider only V' x V',false V' x V, and bool: true if L is simple.", py::return_value_policy::take_ownership);

    Dens.def("between", &DensityAnalyser::between,"Compute the density if the time interval is [beg, end].");
    Dens.def("__str__", [](const DensityAnalyser &d) {
            return toStr(d);
        }
    );
    Dens.def("variableStart",&DensityAnalyser::variableStart,
        "Compute the density if the durartion is duration and the start time varies from beg to end."
    );
    Dens.def("variableDuration",&DensityAnalyser::variableDuration,
        "Compute the density if the begining is at start and if the duration varies from beg to end."
    );
    Dens.def("variableNodes", &DensityAnalyser::variableNodes,
        "Compute the density if the node set is not exactly the same from beg to end."
    );

    Dens.def("__repr__", [](const DensityAnalyser &d) {
            return "<class DensityAnalyser "+toStr(&d)+">";
        }
    );
}

}
