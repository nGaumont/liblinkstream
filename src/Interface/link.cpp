
#include "link.hpp"
#include "interface.hpp"
namespace py = pybind11;

namespace linkstream{

void Link_export(py::module &m){
    py::class_<Link>(m, "Link",R"doc(Link)doc")
    // .def(py::init<const Link &>())
    .def("start",  &Link::start, "Start time of the link")
    .def("stop",  &Link::stop, "Stop time of the link")
    .def("duration",  &Link::duration, "Duration of the link")
    .def("in", &Link::in)
    .def("left", &Link::left, "Return the node on the left.", py::return_value_policy::reference_internal)
    .def("right", &Link::right, "Return the node on the right.", py::return_value_policy::reference_internal)
    .def("otherNode", &Link::otherNode, "Return the other node in the link.", py::return_value_policy::reference_internal)
    .def("sumDegree", &Link::sumDegree, "Return the number of links which have a common node with the link.")
    // .def("links", [](py::object s) {
    //     const Link & l =s.cast<const Link &>();
    //     return PyLinksIterator(l.linkNeighbor().begin(),l.linkNeighbor.end(), s);
    // });
    .def("commonNode", &Link::commonNode, "Return the node in the link.", py::return_value_policy::reference_internal)
    .def("sameNodes", &Link::sameNodes, "Return true if the two links have the same nodes.")
    .def("before", &Link::before)
    .def("after", &Link::after)
    .def("intersection", [](const Link & l1, const Link & l2) -> double { return intersection(l1,l2);})
    .def("__repr__", [](const Link &l) {
            return toStr(l);
        }
    );
}

}
