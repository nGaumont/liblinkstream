#include "tmpLink.hpp"
#include "link.hpp"

using namespace linkstream;

void tmpLink::operator=(const Link & l){
    t=l.start();
    u=l.left().id();
    v=l.right().id();
    dur = l.duration();
}
