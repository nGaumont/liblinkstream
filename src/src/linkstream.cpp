#include "linkstream.hpp"

namespace linkstream{

void linkStream::pushLink(const tmpLink &l) {
  // Create node if it doesn't exist yet;
  Node &left = getCreateNode(l.u);
  Node &right = getCreateNode(l.v);
  if (l.start() <beginning ){
    beginning = l.start();
  }
  if ( l.stop() > ending){
    ending = l.stop();
  }


  linksIterator tmp = links.emplace_hint(links.end(),l, left, right);

  left.push_new(*tmp);
  if (l.u != l.v)
    right.push_new(*tmp);

  // Update events
  if(left.id() < right.id())
    edges[Edge(left.id(), right.id())].push_back(&(*tmp));
  else
    edges[Edge(right.id(), left.id())].push_back(&(*tmp));
}

void linkStream::addLink(const tmpLink &l){
    Node &left = getCreateNode(l.u);
    Node &right = getCreateNode(l.v);
    if (l.start() <beginning ){
      beginning = l.start();
    }
    if ( l.stop() > ending){
      ending = l.stop();
    }

    Link link(l, left, right);
    linksIterator tmp = links.lower_bound(link);
    tmp = links.emplace_hint(tmp,link);
    left.add(*tmp);
    if (l.u != l.v){
      right.add(*tmp);
    }


    // Update events
    if(left.id() < right.id())
      edges[Edge(left.id(), right.id())].push_back(&(*tmp));
    else
      edges[Edge(right.id(), left.id())].push_back(&(*tmp));

}
bool linkStream::removeLink(const Link &l){
    linksIterator tmp = links.lower_bound(l);
    if(!tmp->weakEqual(l)){
        return false;
    }

    (*tmp).left().remove(*tmp);
    (*tmp).right().remove(*tmp);
    nodeID minID = std::min((*tmp).left().id(), (*tmp).right().id());
    nodeID maxID = std::max((*tmp).left().id(), (*tmp).right().id());

    auto activations = edges[ Edge(minID, maxID) ];
    auto it = activations.begin(),
         ite = activations.end();

    it=lower_bound(it,ite,l,plinkRefLinkComp());
    while(it!=ite && (*it)->weakEqual(l)){
        ++it;
    }
    if(it!=ite){
        activations.erase(it);
    }
    links.erase(tmp);
    return true;
}

bool linkStream::removeLink(const tmpLink &l){
    linksIterator tmp = lower_bound(links.begin(), links.end(),l, LinkTmpLinkComp());
    if(!tmp->weakEqual(l)){
        return false;
    }

    (*tmp).left().remove(*tmp);
    (*tmp).right().remove(*tmp);
    nodeID minID = std::min((*tmp).left().id(), (*tmp).right().id());
    nodeID maxID = std::max((*tmp).left().id(), (*tmp).right().id());

    auto activations = edges[ Edge(minID, maxID) ];
    auto it = activations.begin(),
         ite = activations.end();

    it=lower_bound(it,ite,l,pLinkTmpLinkComp());
    while(it!=ite && (*it)->weakEqual(l)){
        ++it;
    }
    if(it!=ite){
        activations.erase(it);
    }
    links.erase(tmp);
    return true;
}




bool linkStream::compLinkList(const linkStream &L)const{
    if (L.links.size() != links.size()){
        std::cout<<"Pas la même taille."<<L.links.size() <<" "<< links.size()<<std::endl;
        return false;
    }
    auto it1=links.begin(), ite1=links.end();
    auto it2=L.links.begin(), ite2=L.links.end();

    for(;it1!=ite1 && it2!=ite2; ++it1, ++it2){
        if(!(*it1).weakEqual(*it2)){
            std::cout<<"Pas le même lien. "<<*it1 <<" et "<< *it2<<std::endl;
            return false;
        }
    }
    return true;
}

bool linkStream::compLinkList(const std::deque<tmpLink> & tmpLinks)const{
    if (tmpLinks.size() != links.size()){
        std::cout<<"Not the same size: "<<tmpLinks.size() <<" "<< links.size()<<std::endl;
        return false;
    }
    auto it1=links.begin(), ite1=links.end();
    auto it2=tmpLinks.begin(),ite2=tmpLinks.end();
    for(;it1!=ite1 && it2!=ite2; ++it1, ++it2){
        if(!(*it1).weakEqual(*it2)){
            std::cout<<"Pas le même lien. "<<*it1 <<" et "<< *it2<<std::endl;
            return false;
        }
    }
    return true;
}

const Link & linkStream::link(linkID lId) const {
    if(static_cast<unsigned int >(lId) >= links.size()){
        std::cerr << "Wrong linkId" << std::endl;
        throw LinkstreamException("link: Wrong linkId");
    }
    auto it = links.begin();
    for(unsigned i =0; i< lId; ++i)
        ++it;
    return *it;
}

const Node & linkStream::node(nodeID nId) const{
  std::map<nodeID, Node>::const_iterator it = nodes.find(nId);
  if (it == nodes.end()) {
    std::cerr << "Node: wrong NodeId" << std::endl;
    throw LinkstreamException("node: wrong NodeId");
  }
  return it->second;
}

Node & linkStream::node(nodeID nId){
  std::map<nodeID, Node>::iterator it = nodes.find(nId);
  if (it == nodes.end()) {
    std::cerr << "Node: wrong NodeId" << std::endl;
    throw LinkstreamException("node: wrong NodeId");
  }
  return it->second;
}

const std::deque<const Link *> & linkStream::edge(nodeID a, nodeID b) const {
  std::map<Edge, std::deque<const Link *> >::const_iterator it =
      edges.find(Edge(a, b));
  if (it == edges.end()) {
      throw LinkstreamException("edge: no link between the nodes.");
  }
  return it->second;
}


bool linkStream::isSimple()const{
    for(auto pair: edges){
        if(pair.second.size()<2){
            continue;
        }
        std::deque<const Link *>::const_iterator it=pair.second.begin(), prev=it,ite=pair.second.end();
        ++it;
        for(;it!=ite;++it){
            if(intersection((**it), **prev)>0){
                return false;
            }
            prev=it;
        }
    }
    return true;
}


linkStream linkStream::makeSimple()const{
    std::deque<tmpLink> links;
    for(auto pair: edges){
        if(pair.second.size()==1){
            links.emplace_back(pair.second[0]->makeTmpLink());
            continue;
        }
        std::deque<const Link *>::const_iterator it=pair.second.begin(), prev=it,ite=pair.second.end();
        ++it;
        tmpLink prev_tmp= (**prev).makeTmpLink();
        for(;it!=ite;++it){
            if(intersection((**it),**prev)>0){
                //Update prev
                prev_tmp.dur = std::max(prev_tmp.stop(), (**it).stop())-prev_tmp.start();
            }else{
                //no intersection we can add safely
                links.emplace_back(prev_tmp);
                prev_tmp= tmpLink((**it).makeTmpLink());
            }
            prev=it;

        }
        links.emplace_back(prev_tmp);
    }
    std::sort(links.begin(),links.end(),[](const tmpLink& l1, const tmpLink& l2){
        if (l1.t<l2.t){
            return true;
        }
        if (l1.t==l2.t){
            if (l1.u> l2.u){
                return true;
            }
            if(l1.v> l2.v){
                return true;
            }
        }
        return false;
    });
    //Now we iterate in time ordered fashion the tmplink.
    // This two step process is needed in order to create correct id
    linkStream L;
    unsigned int count =0;
    for(auto tmpLn: links){
        L.pushLink(tmpLn);
        ++count;
    }
    return L;
}



void linkStream::printDegreeList(std::ostream &f) const {
  const_nodesIterator it = beginNode(), ite = endNode();
  f << (*it).degree();
  ++it;
  for (; it != ite; ++it) {
    f << "," << (*it).degree();
  }
  f << std::endl;
}


void linkStream::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<<"---- LinkStream -----"<<std::endl;
    {
        linkStream L1;
        L1.readFile<TUVD>("test/test1.txt");
        std::cout<<"Test constructor EventFormat::TUVD():"<<std::endl;
        std::cout<<"test1.txt TUVD(): " << singleTest(L1.compLinkList({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                                                   tmpLink(1,nodeID(2),nodeID(3),2),
                                                                   tmpLink(2,nodeID(1),nodeID(2),1),
                                                                   tmpLink(4,nodeID(2),nodeID(3),1.5),
                                                                   tmpLink(5,nodeID(3),nodeID(4),2),
                                                                   tmpLink(6,nodeID(2),nodeID(3),1),})==true, res)<<std::endl;
       std::cout<<"Test isSimple():"<<std::endl;
       std::cout<<"test1.txt isSimple(): " << singleTest(L1.isSimple()==true,res)<<std::endl;
    }
    {
        linkStream L1_BEUV;
        L1_BEUV.readFile<BEUV>("test/test1_beuv.txt");
        std::cout<<"Test constructor EventFormat::BEUV():"<<std::endl;
        std::cout<<"test1.txt BEUV(): " << singleTest(L1_BEUV.compLinkList({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                                                  tmpLink(1,nodeID(2),nodeID(3),2),
                                                                  tmpLink(2,nodeID(1),nodeID(2),1),
                                                                  tmpLink(4,nodeID(2),nodeID(3),1.5),
                                                                  tmpLink(5,nodeID(3),nodeID(4),2),
                                                                  tmpLink(6,nodeID(2),nodeID(3),1),})==true, res)<<std::endl;
    }
    {
        linkStream L3_TUV;
        L3_TUV.readFile<TUV>("test/test1_TUV.txt");
        std::cout<<"Test constructor EventFormat::TUV():"<<std::endl;
        std::cout<<"TUV(test1.txt): " << singleTest(L3_TUV.compLinkList({tmpLink(0,nodeID(1),nodeID(2),0),
                                                                          tmpLink(1,nodeID(2),nodeID(3),0),
                                                                          tmpLink(2,nodeID(2),nodeID(1),0),
                                                                          tmpLink(4,nodeID(2),nodeID(3),0),
                                                                          tmpLink(5,nodeID(3),nodeID(4),0),
                                                                          tmpLink(6,nodeID(2),nodeID(3),0),}), res)<<std::endl;
        linkStream L4_TUV;
        L4_TUV.readFile<TUV>("test/test1_TUV.txt");
        std::cout<<"compLinkList(L3_TUV,L4_TUV)" << singleTest(L4_TUV.compLinkList(L3_TUV), res)<<std::endl;
    }
    {
        linkStream L1_TUV;
        L1_TUV.readFile<TUV>("test/test1_TUV.txt",2);
        std::cout<<"TUV(test1.txt, 2): " << singleTest(L1_TUV.compLinkList({tmpLink(-1,nodeID(1),nodeID(2),2),
                                                                  tmpLink(0,nodeID(2),nodeID(3),2),
                                                                  tmpLink(1,nodeID(2),nodeID(1),2),
                                                                  tmpLink(3,nodeID(2),nodeID(3),2),
                                                                  tmpLink(4,nodeID(3),nodeID(4),2),
                                                                  tmpLink(5,nodeID(2),nodeID(3),2),}),res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        std::cout<<"TUV(test1.txt, 4): " << singleTest(L2_TUV.compLinkList({tmpLink(-2,nodeID(1),nodeID(2),4),
                                                                tmpLink(-1,nodeID(2),nodeID(3),4),
                                                                tmpLink(0,nodeID(2),nodeID(1),4),
                                                                tmpLink(2,nodeID(2),nodeID(3),4),
                                                                tmpLink(3,nodeID(3),nodeID(4),4),
                                                                tmpLink(4,nodeID(2),nodeID(3),4)}),res)<<std::endl;
        std::cout<<"makesimple(L2_TUV): " << singleTest(L2_TUV.makeSimple().compLinkList({tmpLink(-2,nodeID(1),nodeID(2),6),
                                                                tmpLink(-1,nodeID(2),nodeID(3),9),
                                                                tmpLink(3,nodeID(3),nodeID(4),4)}),res)<<std::endl;
    }

    {
        std::cout<<"Test makeSimple():"<<std::endl;
        linkStream L2;
        L2.readFile<TUVD>("test/test2.txt");
        std::cout<<"test2.txt isSimple(): " << singleTest(L2.isSimple()==false,res)<<std::endl;
        linkStream L3;
        L3.readFile<TUVD>("test/test3.txt");
        std::cout<<"makeSimple(L2)==L3: " << singleTest(L2.makeSimple().compLinkList(L3)==true,res)<<std::endl;

        std::cout<<"makeSimple(L3)==L3: " << singleTest(L3.makeSimple().compLinkList(L3)==true,res)<<std::endl;
    }

    {
        linkStream L1;
        L1.readFile<TUVD>("test/test1.txt");
        std::cout<<"link():"<< singleTest((*L1.beginLink()).weakEqual(tmpLink(0,nodeID(1),nodeID(2),1.5)),res)<<std::endl;

        std::cout<<"node():"<< singleTest(L1.node(1).id()==1, res)<<std::endl;
        std::deque<tmpLink> good({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                 tmpLink(1,nodeID(2),nodeID(3),2),
                                 tmpLink(2,nodeID(1),nodeID(2),1),
                                 tmpLink(4,nodeID(2),nodeID(3),1.5),
                                 tmpLink(6,nodeID(2),nodeID(3),1)});
         std::cout<<"node.linksNeighbor(2):"<< singleTest(L1.node(2).weakCompLinkNeighbor(good),res)<<std::endl;

    }

    {
        linkStream L1;
        L1.readFile<TUVD>("test/test1.txt");
        std::deque<tmpLink> good({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                tmpLink(1,nodeID(2),nodeID(3),2),
                                tmpLink(2,nodeID(1),nodeID(2),1),
                                tmpLink(4,nodeID(2),nodeID(3),1.5),
                                tmpLink(6,nodeID(2),nodeID(3),1)});
        std::cout<<"link.linksNeighbor(4):"<< singleTest((*L1.beginLink()).weakCompLinkNeighbor(good),res)<<std::endl;
   }

    {
        linkStream L1;
        L1.readFile<TUVD>("test/test1.txt");
        std::deque<tmpLink> good({tmpLink(1,nodeID(2),nodeID(3),2),
                              tmpLink(4,nodeID(2),nodeID(3),1.5),
                              tmpLink(5,nodeID(3),nodeID(4),2),
                              tmpLink(6,nodeID(2),nodeID(3),1)});
        ;
        std::cout<<"link.linksNeighbor(4):"<< singleTest(L1.link(4).weakCompLinkNeighbor(good),res)<<std::endl;
    }

    {
        linkStream L1,L2;
        L1.readFile<TUVD>("test/test1.txt");

        L2.addLink(tmpLink(0,nodeID(1),nodeID(2),1.5));
        L2.addLink(tmpLink(1,nodeID(2),nodeID(3),2));
        L2.addLink(tmpLink(2,nodeID(1),nodeID(2),1));
        L2.addLink(tmpLink(4,nodeID(2),nodeID(3),1.5));
        L2.addLink(tmpLink(5,nodeID(3),nodeID(4),2));
        L2.addLink(tmpLink(6,nodeID(2),nodeID(3),1));
        std::cout<<"Test addLink():"<<std::endl<<std::endl;
        std::cout<<"addLink(same order): " << singleTest(L1.compLinkList(L2)==true,res)<<std::endl;

        for(auto it = L2.beginNode(); it!= L2.endNode();++it){
            std::cout<<"check node neighbor:" << singleTest((*it).weakCompLinkNeighbor(L1.node((*it).id()).linksNeighbor()), res)<<std::endl;
        }


        linkStream L3;
        L3.addLink(tmpLink(5,nodeID(3),nodeID(4),2));
        L3.addLink(tmpLink(0,nodeID(1),nodeID(2),1.5));
        L3.addLink(tmpLink(6,nodeID(2),nodeID(3),1));
        L3.addLink(tmpLink(1,nodeID(2),nodeID(3),2));
        L3.addLink(tmpLink(2,nodeID(1),nodeID(2),1));
        L3.addLink(tmpLink(4,nodeID(2),nodeID(3),1.5));

        std::cout<<"addLink(random order): " << singleTest(L2.compLinkList(L3)==true,res)<<std::endl;
        for(auto it = L3.beginNode(); it!= L3.endNode();++it){
            std::cout<<"check node neighbor "<<(*it).id()<<": "<< singleTest((*it).weakCompLinkNeighbor(L1.node((*it).id()).linksNeighbor()), res)<<std::endl;
        }
    }

    {
        linkStream L;

        L.addLink(tmpLink(0,nodeID(1),nodeID(2),1.5));
        L.addLink(tmpLink(1,nodeID(2),nodeID(3),2));
        L.addLink(tmpLink(2,nodeID(1),nodeID(2),1));
        L.addLink(tmpLink(4,nodeID(2),nodeID(3),1.5));
        L.addLink(tmpLink(5,nodeID(3),nodeID(4),2));
        L.addLink(tmpLink(6,nodeID(2),nodeID(3),1));

        std::cout<<"remove(tmplink) :"<< singleTest(L.removeLink(tmpLink(4,nodeID(2),nodeID(3),1.5)),res)<<std::endl;

        std::cout<<"test1.txt TUVD(): " << singleTest(L.compLinkList({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                                                   tmpLink(1,nodeID(2),nodeID(3),2),
                                                                   tmpLink(2,nodeID(1),nodeID(2),1),
                                                                   tmpLink(5,nodeID(3),nodeID(4),2),
                                                                   tmpLink(6,nodeID(2),nodeID(3),1),})==true, res)<<std::endl;
        std::cout<<"remove(link) :"<< singleTest(L.removeLink(L.link(2)),res)<<std::endl;
        std::cout<<"test1.txt TUVD(): " << singleTest(L.compLinkList({tmpLink(0,nodeID(1),nodeID(2),1.5),
                                                                   tmpLink(1,nodeID(2),nodeID(3),2),
                                                                   tmpLink(5,nodeID(3),nodeID(4),2),
                                                                   tmpLink(6,nodeID(2),nodeID(3),1),})==true, res)<<std::endl;

    }
        std::cout<< "End LinkStream test."<<std::endl;
}

}
