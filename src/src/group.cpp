#include "group.hpp"

using namespace linkstream;
bool Group::add(const Link & l){
    bool res = link_set.insert(&l).second;
    if (res){
        updateNodeset = false;
        updateTimeSpan = false;
    }
    return res;
}

bool Group::add_hint(LinkSet::iterator hint, const Link & l){
    unsigned int old_size = link_set.size();
    link_set.insert(hint, &l);
    if (old_size != link_set.size()){
        updateNodeset = false;
        updateTimeSpan = false;
        return true;
    }
    return false;
}

bool Group::remove(const Link & l){
    bool res = static_cast<bool>(link_set.erase(&l));
    if (res){
        updateNodeset = false;
        updateTimeSpan = false;
    }
    return res;
}

const NodeSet & Group::inducedNode()const{
    if(!updateNodeset){
        node_set.clear();
            for(const Link * l: link_set){
                node_set.insert(&l->left());
                node_set.insert(&l->right());
        }
        updateNodeset = true;
    }
    return node_set;
}

void Group::updateTime()const{
    auto it = link_set.begin(), ite=link_set.end();
    const Link * cur_max = *it;
    for(;it!=ite;++it) {
        if ((*it)->stop() > (cur_max->stop())) {
            cur_max = *it;
        }
    }
    updateTimeSpan = true;
    ending = cur_max->stop();
}


bool Group::contains(const Link & l)const{
    return link_set.find(&l) != link_set.end();
}


void Group::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<<"---- Begin Group -----"<<std::endl;
    linkStream L;
    L.readFile<TUVD>("test/test1.txt");

    Group g;
    g.add(L.link(0));

    std::cout<<"start():"<<singleTest(g.start()==0,res) <<std::endl;
    std::cout<<"stop():"<<singleTest(g.stop()==1.5,res) <<std::endl;
    std::cout<<"duration():"<<singleTest(g.duration()==1.5,res) <<std::endl;

    g.add(L.link(1));
    std::cout<<"start():"<<singleTest(g.start()==0,res) <<std::endl;
    std::cout<<"stop():"<<singleTest(g.stop()==3,res) <<std::endl;
    std::cout<<"duration():"<<singleTest(g.duration()==3,res) <<std::endl;
    {
        std::set<nodeID> tmp;
        for(auto it_n = g.beginNode(); it_n!=g.endNode();++it_n){
            tmp.insert((*it_n).id());
        }
        std::cout<<"nodeSet():"<<singleTest(tmp==std::set<nodeID>({nodeID(1),nodeID(2),nodeID(3)}),res)<<std::endl;
    }

    g.remove(L.link(0));
    std::cout<<"start():"<<singleTest(g.start()==1,res) <<std::endl;
    std::cout<<"stop():"<<singleTest(g.stop()==3,res) <<std::endl;
    std::cout<<"duration():"<<singleTest(g.duration()==2,res) <<std::endl;
    {
        std::set<nodeID> tmp;
        for(auto it_n = g.beginNode(); it_n!=g.endNode();++it_n){
            tmp.insert((*it_n).id());
        }
        std::cout<<"nodeSet():"<<singleTest(tmp==std::set<nodeID>({nodeID(2),nodeID(3)}),res)<<std::endl;
    }
    std::cout<<"---- End Group -----"<<std::endl;
}
