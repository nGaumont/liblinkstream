#include "profil.hpp"
using namespace linkstream;

double linkstream::density(double duration, double sum_deg, unsigned int nodeSize){
    if (duration == 0){
        return 0;
    }
    double max_link = ((nodeSize-1.0)*nodeSize)/2.0;
    return 1.0/max_link * sum_deg/(duration);
}

std::ostream& linkstream::operator<<(std::ostream & out, const Profil & p){
    out<<"[";
    for(auto it= p.begin();it!= p.end();++it){
        out<<*it<<", ";
    }
    out<<"]";
    return out;
}
void Profil::changementFrom(std::deque<const Link *>::const_iterator beg, std::deque<const Link *>::const_iterator end, std::map<timestamp, int> & changes)const{
    tmpLink prev_tmp;
    prev_tmp=(**beg);
    ++beg;
    for(;beg!=end;++beg){
        if(intersection(prev_tmp,(**beg))>0){
            //Update prev
            prev_tmp.dur = std::max(prev_tmp.stop(), (**beg).stop())-prev_tmp.t;
        }else{
            increment(changes, prev_tmp.t, 1);
            decrement(changes, prev_tmp.stop(), 1);
            prev_tmp.t=(**beg).start();
            prev_tmp.dur=(**beg).duration();
        }
    }
    increment(changes, prev_tmp.t, 1);
    decrement(changes, prev_tmp.stop(), 1);
}

void Profil::fillProfil(const std::map<timestamp, int> & changes, bool clear_profil){
    if (changes.empty()){
        return;
    }
    if(clear_profil){
        profil.clear();
    }
    std::map<timestamp, int>::const_iterator prev=changes.begin(),cur=prev ;
    ++cur;
    unsigned int cur_deg=0;
    for(;cur!= changes.end();++cur){
        cur_deg += (*prev).second;
        profil.emplace_hint(profil.end(), (*prev).first, Profil::Block((*prev).first, (*cur).first-(*prev).first,cur_deg));
        prev=cur;
    }
    cur_deg += (*prev).second;
    if(cur_deg !=0){
        std::cerr<<"There is a problem with the set of changement provided."<<std::endl;
        exit(-1);
    }
}


std::map<timestamp, int> Profil::activations(const linkStream & L, const std::set<nodeID> & nodes)const{
    std::map<timestamp, int> changes;
    for (auto it = nodes.begin(); it != nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            try{
                auto events = L.edge(*it, *it2); // get all the temporal edges between it and it2
                                                 // it throws an error if not link exist.
                for (auto it = events.begin() ; it !=events.end(); ++it) {
                    increment(changes, (**it).start(), 1);
                    decrement(changes, (**it).stop(), 1);
                }
            }catch (std::exception& e){
                // This an ugly code sorry.
            }
        }
    }
    return changes;
}

std::map<timestamp, int> Profil::extended_activations(const linkStream & L, const std::set<nodeID> & nodes)const{
    std::map<timestamp, int> changes;
    for (auto it1 = L.beginNode(); it1 != L.endNode(); ++it1) {
        auto it2 = nodes.begin();
        for (; it2 != nodes.end(); ++it2) {

            if( (*it2==(*it1).id()) || (*it2<=(*it1).id() && nodes.find((*it1).id())!= nodes.end())){
                continue;
            }
            nodeID minID= std::min((*it1).id(),(*it2)), maxID = std::max(*(it2),(*it1).id());
            // EventList doit etre appelé avec it < it2
            try{
                auto events = L.edge(minID, maxID); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.

                for (auto it = events.begin() ; it !=events.end(); ++it) {
                    increment(changes, (**it).start(), 1);
                    decrement(changes, (**it).stop(), 1);
                }
            }catch (std::exception& e){
                // This an ugly code sorry.
            }
        }
    }
    return changes;
}

std::map<timestamp, int> Profil::activations_notSimple(const linkStream & L, const std::set<nodeID> & nodes)const{
    std::map<timestamp, int> changes;
    for (auto itN = nodes.begin(); itN != nodes.end(); ++itN) {
        auto itN2 = itN;
        // I don't consider self loops.
        ++itN2;
        for (; itN2 != nodes.end(); ++itN2) {
            // EventList doit etre appelé avec it < it2
            try{
                std::cout<< *itN<<" "<<*itN2<<std::endl;

                auto events = L.edge(*itN, (*itN2)); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.

                auto itEvent = events.begin(), itEventE =events.end();
                changementFrom(itEvent, itEventE, changes);
            }catch (std::exception& e){
                // This an ugly code sorry.
            }
        }
    }
    return changes;
}




std::map<timestamp, int> Profil::extended_activations_notSimple(const linkStream & L, const std::set<nodeID> & nodes)const{
    std::map<timestamp, int> changes;
    for (auto it1 = L.beginNode(); it1 != L.endNode(); ++it1) {
        auto it2 = nodes.begin();
        for (; it2 != nodes.end(); ++it2) {

            if( (*it2==(*it1).id()) || (*it2<=(*it1).id() && nodes.find((*it1).id())!= nodes.end())){
                continue;
            }

            nodeID minID= std::min((*it1).id(),(*it2)), maxID = std::max(*(it2),(*it1).id());
            // EventList doit etre appelé avec it < it2
            try{
                auto events = L.edge(minID, maxID); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.

                auto itEvent = events.begin(), itEventE =events.end();
                changementFrom(itEvent, itEventE, changes);
            }catch (std::exception& e){
                // This an ugly code sorry.
            }
        }
    }
    return changes;
}

std::map<timestamp, int> Profil::inter_activations(const linkStream & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const{
    std::map<timestamp, int> changes;
    timestamp max_g = std::max(g1.stop(), g2.stop());
    timestamp min_g = std::min(g1.start(), g2.start());
    for (auto it = union_nodes.begin(); it != union_nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != union_nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            try{
                auto events = L.edge(*it, *it2); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.

                for (auto link_it = events.begin() ; link_it !=events.end(); ++link_it) {

                    if((**link_it).start()> max_g){
                        break;
                    }
                    if((**link_it).stop()< min_g){
                        continue;
                    }
                    if(!g1.contains(**link_it) &&  !g2.contains(**link_it)){
                        timestamp cur_min= std::max(min_g,(**link_it).start());
                        timestamp cur_max= std::min(max_g,(**link_it).stop());
                        increment(changes, cur_min, 1);
                        decrement(changes, cur_max, 1);
                    }
                }
            }catch (std::exception& e){
                // This an ugly code sorry.
            }
        }
    }
    return changes;
}

std::map<timestamp, int> Profil::activations(const Group & g)const{
    Group::const_linksIterator it= g.beginLink(), ite = g.endLink();
    std::map<timestamp, int> changes;
    for(;it!=ite;++it){
        increment(changes, (*it).start(), 1);
        decrement(changes, (*it).stop(), 1);
    }
    return changes;
}

std::map<timestamp, int> Profil::activations_notSimple(const Group & g)const{
    std::map<Edge, tmpLink, edgeComp> events;
    std::map<timestamp, int> changes;
    Group::const_linksIterator it= g.beginLink(), ite = g.endLink();
    for(;it!=ite;++it){
        nodeID minID=std::min((*it).left().id(),(*it).right().id()), maxID=std::max((*it).left().id(),(*it).right().id());
        tmpLink & last = events[Edge(minID,maxID)];
        if(last.empty()){
            // First time I see the pair
            last=(*it);
        }else{
            assert((*it).start()>= last.start());
            if((*it).start()<=last.stop()){
                //Just have to maj the last appearance
                last.dur= std::max((*it).stop(), last.stop()) - last.t;
            }else{
                increment(changes, last.start(), 1);
                decrement(changes, last.stop(), 1);
                last=(*it);
            }
        }
    }

    //Now inside events are last appearance of each (u,v) which have to be stored.
    for(auto pair: events){
        increment(changes, pair.second.start(), 1);
        decrement(changes, pair.second.stop(), 1);
    }
    return changes;
}

std::map<timestamp, int> Profil::inter_activations_notSimple(const linkStream & L, const Group & g1, const Group &g2, const std::set<nodeID> & union_nodes)const{
    std::map<timestamp, int> changes;
    std::map<Edge, tmpLink, edgeComp> appear;
    timestamp max_g = std::max(g1.stop(), g2.stop());
    timestamp min_g = std::min(g1.start(), g2.start());
    for (auto it = union_nodes.begin(); it != union_nodes.end(); ++it) {
        auto it2 = it;
        // I don't consider self loops.
        ++it2;
        for (; it2 != union_nodes.end(); ++it2) {
            // EventList doit etre appelé avec it < it2
            try{
                auto events = L.edge(*it, *it2); // get all the temporal edges between it and it2
                // first is a bool wether they exist or not.
                // second is an actual links container.

                for (auto link_it = events.begin() ; link_it !=events.end(); ++link_it) {
                    if((**link_it).start()> max_g){
                        break;
                    }
                    if((**link_it).stop()< min_g){
                        continue;
                    }
                    if(!g1.contains(**link_it) &&  !g2.contains(**link_it)){
                        timestamp cur_min= std::max(min_g,(**link_it).start() );
                        timestamp cur_max= std::min(max_g,(**link_it).stop());
                        tmpLink & last = appear[Edge(*it, *it2)];
                        if(last.empty()){
                            // First time I see the pair
                            last=(**link_it);
                            last.t=cur_min;
                            last.dur= cur_max - cur_min;
                        }else{
                            assert(cur_min>= last.t);
                            if(cur_min<=last.stop()){
                                //Just have to maj the last appearance
                                last.dur= std::max(cur_max, last.stop()) - last.t;
                            }else{
                                increment(changes, last.start(), 1);
                                decrement(changes, last.stop(), 1);
                                last=(**link_it);
                                last.t=cur_min;
                                last.dur= cur_max - cur_min;
                            }
                        }
                    }
                }
            }catch (std::exception& e){
                // This an ugly code sorry.
            }

        }
    }
    //Now inside events are last appearance of each (u,v) which have to be stored.
    for(auto pair: appear){
        increment(changes, pair.second.start(), 1);
        decrement(changes, pair.second.stop(), 1);
    }
    return changes;
}
Profil::Profil(const Group & g, bool assume_simple){
    std::map<timestamp, int> changes;
    if(assume_simple){
        changes = activations(g);
    }else{
        changes = activations_notSimple( g);
    }

    fillProfil(changes);
}

Profil::Profil(const linkStream & L, double delta){
    std::map<timestamp, int> changes;
    auto it=L.beginLink(),ite=L.endLink();
    timestamp min_Life= L.start(),max_Life= L.stop();
    for(;it!=ite;++it){
        increment(changes, std::min((*it).start()-delta/2, min_Life), 1);
        decrement(changes, std::max((*it).stop() + delta/2, max_Life), 1);
    }
    fillProfil(changes);
}
Profil::Profil(const Group & g, double delta){
    std::map<timestamp, int> changes;
    auto it= g.beginLink(), ite = g.endLink();
    timestamp min_Life= g.start(),max_Life= g.stop();
    for(;it!=ite;++it){
        increment(changes, std::min((*it).start()-delta/2, min_Life), 1);
        decrement(changes, std::max((*it).stop() + delta/2, max_Life), 1);
    }
    fillProfil(changes);
}
Profil::Profil(const linkStream & L, const std::set<nodeID> & nodes, double delta, bool incluse){
    std::map<timestamp, int> changes;
    timestamp min_Life= L.start(),max_Life= L.stop();
    if(incluse){
        for (auto itN = nodes.begin(); itN != nodes.end(); ++itN) {
            auto itN2 = itN;
            // I don't consider self loops.
            ++itN2;
            for (; itN2 != nodes.end(); ++itN2) {
                // EventList doit etre appelé avec it < it2
                try{
                    auto events = L.edge(*itN, (*itN2)); // get all the temporal edges between it and it2
                    // first is a bool wether they exist or not.
                    // second is an actual links container.
                    for (auto it = events.begin() ; it !=events.end(); ++it) {
                        increment(changes, std::min((**it).start()-delta/2, min_Life), 1);
                        decrement(changes, std::max((**it).stop() + delta/2, max_Life), 1);
                    }
                }catch (std::exception& e){
                    // This an ugly code sorry.
                }
            }
        }
    }
    else{
        for (auto it1 = L.beginNode(); it1 != L.endNode(); ++it1) {
            auto it2 = nodes.begin();
            for (; it2 != nodes.end(); ++it2) {

                if( (*it2==(*it1).id()) || (*it2<=(*it1).id() && nodes.find((*it1).id())!= nodes.end())){
                    continue;
                }
                nodeID minID= std::min((*it1).id(),(*it2)), maxID = std::max(*(it2),(*it1).id());
                // EventList doit etre appelé avec it < it2
                try{
                    auto events = L.edge(minID, maxID); // get all the temporal edges between it and it2
                    // first is a bool wether they exist or not.
                    // second is an actual links container.
                    for (auto it = events.begin() ; it !=events.end(); ++it) {
                        increment(changes, std::min((**it).start() - delta/2, min_Life), 1);
                        decrement(changes, std::max((**it).stop() + delta/2, max_Life), 1);
                    }
                }catch (std::exception& e){
                    // This an ugly code sorry.
                }
            }
        }
    }

    fillProfil(changes);
}


Profil::Profil(const linkStream & L, bool assume_simple){
    std::map<timestamp, int> changes;
    if(assume_simple){
        auto it=L.beginLink(),ite=L.endLink();
        for(;it!=ite;++it){
            increment(changes, (*it).start(), 1);
            decrement(changes, (*it).stop(), 1);
        }
    }else{
        auto itEvent=L.beginEdge(), iteEvent=L.endEdge();
        for(;itEvent!=iteEvent;++itEvent){
            changementFrom((*itEvent).begin(), (*itEvent).end(), changes);
        }
    }

    fillProfil(changes);
}


Profil::Profil(const std::deque<Link *> & links):profil(){
    auto it = links.begin(), ite = links.end();
    // timestamp last_begin = 0;
    timestamp prev_end=0;
    for (; it != ite; ++it) {
        const Link &cur = **it;
        if(cur.stop()<prev_end){
            std::cerr<<"The linkstream is not simple."<<std::endl;
            exit(-1);
        }
        // std::cout<<std::setprecision(16)<<cur<<std::endl;
        prev_end = cur.stop();
        profil.emplace_hint(profil.end(), cur.start(), cur);
    }
}


Profil::Profil(timestamp beg, timestamp end, double val):profil(){
    profil.emplace_hint(profil.end(), beg, Block(beg, end-beg, val));
}
Profil::Profil(const linkStream & L, const std::set<const Node *, nodeComparator> & nodes, bool incluse, bool assume_simple){
    std::set<nodeID> ids;
    for(auto n : nodes){
        ids.insert(ids.end(), (*n).id());
    }
    std::map<timestamp, int> changes;
    if(assume_simple){
        if(incluse){
            changes = activations(L, ids);
        }else{
            changes = extended_activations(L,ids);
        }
    }else{
        if(incluse){
            changes = activations_notSimple(L, ids);
        }else{
            changes = extended_activations_notSimple(L,ids);
        }
    }

    fillProfil(changes);
}
Profil::Profil(const linkStream & L, const std::set<nodeID> & nodes, bool incluse, bool assume_simple){
    std::map<timestamp, int> changes;
    if(assume_simple){
        if(incluse){
            changes = activations(L, nodes);
        }else{
            changes = extended_activations(L,nodes);
        }
    }else{
        if(incluse){
            changes = activations_notSimple(L, nodes);
        }else{
            changes = extended_activations_notSimple(L,nodes);
        }
    }
    fillProfil(changes);
}

bool Profil::operator==(const Profil & p) const{
    if(profil.size() != p.profil.size()){
        return false;
    }
    Profil::const_iterator cur1=begin(), cur2=p.begin(),
                     end1=end(),end2=p.end();
    while(cur1!=end1 && cur2 !=end2){
        if((*cur1)!=(*cur2)){
            // std::cout<<"Dif:"<<(*cur1)<<" "<<(*cur2)<<std::endl;
            // std::cout<<std::setprecision(48);
            // std::cout<<RESOLUTION<<std::endl;
            // std::cout<<(*cur1).t<<" "<<(*cur2).t<<" "<<(*cur1).t - (*cur2).t<<std::endl;
            // std::cout<<(*cur1).d<<" "<<(*cur2).d<<" "<<(*cur1).d-(*cur2).d<<std::endl;
            // std::cout<<(*cur1).val<<" "<<(*cur2).val<<" "<<(*cur1).val-(*cur2).val<<std::endl;
            return false;
        }
        ++cur1;
        ++cur2;
    }
    return true;
}

void Profil::add(const Profil & p){
    this->modifier(p,[](const Block & x, const Block & y){return x.val+y.val;});
}

Profil::iterator Profil::modifier_hint(Profil::iterator hint, Block b, std::function<double (const Block &, const Block &)> func){
    // assert(endLife() >= b.stop());
    if(!(*hint).intersect(b)){
        std::cerr<< std::setprecision(48);
        std::cerr<<"Wrong hint "<<*hint<<" "<<b<<std::endl;
        std::cerr<<"Wrong hint "<<b<<std::endl;
        std::cerr<<" "<<(*hint).isconsistent()<<std::endl;
        std::cerr<<" "<<(*this).isconsistent()<<std::endl;
        std::cerr<<" "<<(*this)<<std::endl;
        std::cerr<<"Wrong hint "<<(*hint).t-b.t<<" "<<(*hint).stop()-b.t <<std::endl;
        exit(-1);
    }
    timestamp time_dif = b.t-(*hint).t;
    // b and hint don't begin dont begin at the same time.
    if(time_dif > RESOLUTION){
        //split hint
        double last_val = (*hint).val;
        timestamp last_end = (*hint).stop();
        assert(last_end-b.t > 0);
        (*hint).d=time_dif;
        ++hint;
        hint = profil.emplace_hint(hint.raw(), b.t, Block(b.t, last_end-b.t, last_val));

    }
    // now hint and b have the same start time.
    // Only have to update val for block which are inside the time interval from b.
    while( hint!=end()&& (*hint).stop() <= b.stop()){
        (*hint).val = func(*hint, b);
        ++hint;
    }

    //Now hint migh outlive and has been mistakenly updated
    // first check that b is not already finsished then
    if( hint!=end() && b.stop()-(*hint).t> RESOLUTION  ){
        double last_val = (*hint).val;

        assert(b.stop()-(*hint).t > 0);
        (*hint).val = func(*hint, b);
        timestamp last_dur = (*hint).stop()-b.stop();
        assert(last_dur > 0);
        if (last_dur>RESOLUTION){
            (*hint).d = b.stop()-(*hint).t;
            ++hint;
            hint = profil.emplace_hint(hint.raw(), b.stop(), Block(b.stop(), last_dur, last_val));
        }else{
            ++hint;
        }
    }

    return hint;
}
void Profil::modifier(const Profil & p, std::function<double (const Block &, const Block &)> func){
    // assert(this->isconsistent());
    // assert(p.isconsistent());
    Profil::const_iterator pit =p.begin(), pite=p.end();
    Profil::iterator cur=at((*pit).t); // Return closest block to t.
    pit = p.at((*cur).t,false);
    while(pit != pite && cur != end()){
        cur = modifier_hint(cur,*pit, func);
        ++pit;
        // advance cur to the next intersction
        while((*cur).stop()<= (*pit).t && cur != end()){
            ++cur;
        }
    }
    // std::cout<<"Result profil:"<<std::endl;
    // std::cout<<*this<<std::endl;
}

void Profil::modifier(std::function<double (const Block &)> func){
    assert(this->isconsistent());
    Profil::iterator it =begin(), ite=end();
    while(it != ite){
        (*it).val = func(*it);
        ++it;
    }
}

double Profil::AUC(timestamp beg, timestamp end)const{
    if(beg> end){
        return 0.0;
    }
    assert(end>beg);
    if((*this).size() == 0 || end <= start() || beg >= stop() ){
        return 0;
    }
    Profil::const_iterator cur = at(beg,false);
    double acc = 0.0;
    if(end <= (*cur).stop()){
        return (*cur).val *(std::min(end,(*cur).stop())-std::max(beg,(*cur).start()));
    }

    // The first block is used only in the interval [max(beg,cur.t), cur.stop]
    acc += (*cur).val * ((*cur).stop() - std::max(beg,(*cur).start()));
    ++cur;
    while(cur != profil.end() && (*cur).stop() < end){
        acc += (*cur).val* (*cur).d ;
        ++cur;
    }
    if(cur != profil.end()){
        // the last block is also only used in the interval [cur.t; end]
        acc += (*cur).val * (std::min(end,(*cur).stop())  - (*cur).start());
    }
    return acc;
}


void Profil::dump()const{
    Profil::const_iterator it = begin(), ite = end();
    for(;it!=ite;++it){
        std::cout<<(*it).t<<" "<<(*it).val<<std::endl;
        std::cout<<(*it).stop()<<" "<<(*it).val<<std::endl;
    }
}

double Profil::percentil_pointwise(double threshold)const{
    assert(size()>=2);
    double res =0;
    Profil::const_iterator it = begin(), next=it, ite=end();
    ++next;
    for(;next!=ite;++next){
        // The block represent has a value from b.val to next.val in the interval of b.
        const Block & b=*it;
        const Block & be=(*next);

        if(b.val < threshold){
            if(be.val >= threshold){
                // b is under and next is above
                double intersect_x = (be.t-b.t)*(threshold-b.val)/(be.val-b.val) + b.t;
                res+= be.t-intersect_x;
            }
            // If both are under -> nothing to do
        }else{
            if(be.val < threshold){
                // b is above and next under
                double intersect_x = (be.t-b.t)*(threshold-b.val)/(be.val-b.val) + b.t;
                res+= intersect_x-b.t;
            }else{
                // Both  b and next are above
                res+= b.d;
            }
        }
        it=next;
    }
    res = 1-res/(duration());
    return res;
}

double Profil::percentil_stepFunction(double threshold)const{
    assert(size()>=2);
    double res =0;
    Profil::const_iterator it = begin(), ite=end();
    for(;it!=ite;++it){
        // The block represent has a value from b.val to next.val in the interval of b.
        const Block & b=*it;
        if(b.val >= threshold){
                // Both  b and next are above
                res+= b.d;
        }
    }
    res = 1-res/(duration());
    return res;
}

bool Profil::isconsistent()const{
    if(size()==0){
        return true;
    }
    if(size()==1){
        return (*begin()).isconsistent();
    }
    Profil::const_iterator it = begin(), next=it, ite=end();
    ++next;
    for(;next!=ite;++next){
        if( ! (*it).isconsistent() || std::abs((*it).stop()-(*next).t)>RESOLUTION ){
            std::cout<<(*it)<<" and "<<*next<<" are not consistent."<<std::endl;
            return false;
        }
        it=next;
    }
    if(!(*it).isconsistent()){
        std::cout<<(*it)<<" is not consistent. "<<std::isfinite((*it).val)<<std::endl;
    }

    return (*it).isconsistent();
}


void Profil::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<<"---- Begin Profil -----"<<std::endl;

    linkStream L;
    L.readFile<TUVD>("test/test1.txt");
    Partition C("test/aff_test1.txt", L);
    std::cout<<std::setprecision(16);
    std::cout<<"Test activation:"<<std::endl;
    {
        Profil p= Profil();
        std::map<timestamp, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5,1}, {5.5,-1}, {6,1}, {7,-2}};
        std::cout<<"activation(L, nodes):" << singleTest(p.activations(L, {nodeID(1),nodeID(2),nodeID(3),nodeID(4)})==tmp,res)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"fillProfil():" << singleTest(p==tmp2,res)<<std::endl;
        Profil p2(L, {nodeID(1),nodeID(2),nodeID(3),nodeID(4)}, true, true);
        Profil p3(L,true);
        Profil p4(L, false);
    }
    {
        Profil p= Profil();
        std::map<timestamp, int> tmp = {{1,1}, {3,-1}, {4,1}, {5,1}, {5.5,-1}, {6,1}, {7,-2}};
        std::cout<<"activation(L, nodes-1)_bis:" << singleTest(p.activations(L, {nodeID(2),nodeID(3),nodeID(4)})==tmp,res)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{1, Profil::Block(1,2,1)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"fillProfil()_bis:" << singleTest(p==tmp2,res)<<std::endl;
        Profil p2(L, {nodeID(2),nodeID(3),nodeID(4)}, true, true);
        std::cout<<"Profil(L,nodes)_bis:" << singleTest(p==p2,res)<<std::endl;
    }
    {
        Profil p= Profil();
        std::map<timestamp, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5.5,-1}, {6,1}, {7,-1}};
        std::cout<<"activation(L, nodes-1):" << singleTest(p.activations(L, {nodeID(1),nodeID(2),nodeID(3)})==tmp,res)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0)},
                              {6, Profil::Block(6,1,1)},
                          });
        std::cout<<"fillProfil():" << singleTest(p==tmp2,res)<<std::endl;
        Profil p2(L, {nodeID(1),nodeID(2),nodeID(3)}, true, true);
        std::cout<<"Profil(L,nodeID):" << singleTest(p==p2,res)<<std::endl;
        Profil p3(L, {&L.node(1),&L.node(2), &L.node(3)}, true, true);
        std::cout<<"Profil(L,nodes):" << singleTest(p3==p2,res)<<std::endl;
    }
    {
        Profil p= Profil();
        std::map<timestamp, int> tmp = {{0,1}, {1,1}, {1.5,-1}, {2,1}, {3,-2}, {4,1}, {5.5,-1}, {6,1}, {7,-1}};
        std::cout<<"extended_activation(L, nodes-2):" << singleTest(p.extended_activations(L, {nodeID(1),nodeID(2)})==tmp,res)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0)},
                              {6, Profil::Block(6,1,1)},
                          });
        std::cout<<"fillProfil():" << singleTest(p==tmp2,res)<<std::endl;
        Profil p2(L, std::set<nodeID>({nodeID(1),nodeID(2)}), false, true);
        std::cout<<"Profil(L,nodes,false, true):" << singleTest(p==p2,res)<<std::endl;
    }

    {
        linkStream L;
        L.readFile<TUVD>("test/test2.txt");
        std::cout<<std::setprecision(16);
        std::set<nodeID> nodes= {nodeID(2), nodeID(3)};
        Profil p1(L, nodes, false, false);
        std::cout<<"Profil(L,nodes,false, false):" << singleTest(p1==Profil(L,false),res)<<std::endl;
    }
    {
        linkStream L;
        L.readFile<TUVD>("test/test2.txt");
        std::cout<<std::setprecision(16);
        std::set<nodeID> nodes= {nodeID(1), nodeID(2)};
        Profil p1(L, nodes, true, false);
        Profil tmp1 =  Profil({{0, Profil::Block(0,3,1.0)},
                          });
        std::cout<<"Profil(L,nodes,true, false):" << singleTest(p1==tmp1,res)<<std::endl;
    }

    {
        Profil p(C.group(0));
        Profil tmp2 =  Profil({{4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"Profil(L, group):" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        Partition C2("test/aff_test2.txt", L);
        Profil p;
        std::map<timestamp, int> tmp = {{2,1}, {3,-1}, {4,1}, {5.5,-1}},
                                   tmp_res = p.inter_activations(L, C2.group(0), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        std::cout<<"activation(L,inter):" << singleTest(tmp_res==tmp, res)<<std::endl;
        p.fillProfil(tmp);
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1.5,1)},
                          });
        Profil p2 = Profil::inter(L, C2.group(0), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        std::cout<<"Profil::inter:" << singleTest(p==p2, res)<<std::endl;
    }
    {
        Partition C2("test/aff_test2.txt", L);
        Profil p = Profil::inter(L, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)});
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)}
                          });
        std::cout<<"Profil::inter:" << singleTest(p==tmp2, res)<<std::endl;
    }
    {
        Partition C2("test/aff_test2.txt", L);
        Profil p(C2.group(1),false);
        Profil p2(C2.group(1),true);
        std::cout<<"Profil::Profil(L,group,false):" << singleTest(p==p2,res)<<std::endl;
    }
    {
        linkStream L1_TUV;
        L1_TUV.readFile<TUV>("test/test1_TUV.txt",2);
        Partition C1_TUV("test/aff_test1.txt", L1_TUV);
        Profil p(C1_TUV.group(1),false);
        Profil tmp2 =  Profil({{-1, Profil::Block(-1,1,1)},
                              {0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,1,1)},
                          });
        std::cout<<"Profil::notsimple activation_1:" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Partition C2_TUV("test/aff_test2.txt", L2_TUV);
        Profil p(C2_TUV.group(1),false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,1)},
                              {2, Profil::Block(2,2,2)},
                              {4, Profil::Block(4,2,1)}
                          });
        std::cout<<"Profil::notsimple activation_2:" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Profil p(L2_TUV,false);
        Profil p2(L2_TUV.makeSimple(),true);
        std::cout<<"Profil::Profil(L, false):" << singleTest(p==p2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",1);
        Partition C2("test/aff_test2.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},true);
        Profil p2 = Profil::inter(L2_TUV, C2.group(1), C2.group(2), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},false);
        std::cout<<"Profil::inter not simple_1:" << singleTest(p==p2,res)<<std::endl;
        // std::cout<<"Profil::inter_activations_notSimple:" << singleTest(res==tmp,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUVD>("test/test1.txt");
        Partition C3("test/aff_test3.txt", L2_TUV);
        Profil p(C3.group(2),true);
        Profil tmp2 =  Profil({{2, Profil::Block(2,1,1)}
                          });
        std::cout<<"Profil(group):" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Partition C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(4), {nodeID(1),nodeID(2),nodeID(3),nodeID(4)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,5,1)}
                          });
        std::cout<<"Profil::inter not simple_2:" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test1_TUV.txt",4);
        Partition C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(5), {nodeID(1),nodeID(2),nodeID(3)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,4,1)}
                          });
        std::cout<<"Profil::inter not simple_3:" << singleTest(p==tmp2,res)<<std::endl;
    }
    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUV>("test/test2_TUV.txt",4);
        Partition C3("test/aff_test3.txt", L2_TUV);
        Profil p = Profil::inter(L2_TUV, C3.group(2), C3.group(5), {nodeID(1),nodeID(2),nodeID(3)},false);
        Profil tmp2 =  Profil({{0, Profil::Block(0,2,2)},
                              {2, Profil::Block(2,4,1)},
                          });
        std::cout<<"Profil::inter not simple_4:" << singleTest(p==tmp2,res)<<std::endl;
    }

    {
        linkStream L2_TUV;
        L2_TUV.readFile<TUVD>("test/test1.txt");
        Profil p(L2_TUV,true);
        std::cout<<"Profil::start():" << singleTest(p.start()==0,res)<<std::endl;
        std::cout<<"Profil::stop():" << singleTest(p.stop()==7,res)<<std::endl;
        std::cout<<"Profil::duration():" << singleTest(p.duration()==7,res)<<std::endl;


        std::cout<<"Test at:"<<std::endl;
        std::cout<<"at(0):" << singleTest(*p.at(0)==Profil::Block(0,1.0,1.0),res)<<std::endl;
        std::cout<<"at(-3):" << singleTest(*p.at(-3)==Profil::Block(0,1.0,1.0),res)<<std::endl;
        std::cout<<"at(0.5):" << singleTest(*p.at(0.5)==Profil::Block(0,1.0,1.0),res)<<std::endl;
        std::cout<<"at(0.5,false):"<< singleTest(*p.at(0.5,false)==Profil::Block(0,1.0,1.0),res)<<std::endl;
        std::cout<<"at(1):"<< singleTest(*p.at(1)==Profil::Block(0,1.0,1.0),res)<<std::endl;
        std::cout<<"at(1,false):"<< singleTest(*p.at(1,false)==Profil::Block(1,0.5,2.0),res)<<std::endl;
        std::cout<<"at(7):"<< singleTest(*p.at(7)==Profil::Block(6,1,2.0),res)<<std::endl;
        std::cout<<"at(7,false):"<< singleTest(*p.at(7,false)==Profil::Block(6,1,2.0),res)<<std::endl;
        std::cout<<"at(8):"<< singleTest(*p.at(8)==Profil::Block(6,1,2.0),res)<<std::endl;
        std::cout<<"at(8,false):"<< singleTest(*p.at(8,false)==Profil::Block(6,1,2.0),res)<<std::endl;
        std::cout<<std::endl;

        std::cout<<"Test AUC:"<<std::endl;
        std::cout<<"AUC[0;7]:"<< singleTest(p.AUC(0,7) == 9,res)<<std::endl;
        std::cout<<"AUC[-3;42]:"<< singleTest(p.AUC(-3,42) == 9,res)<<std::endl;
        std::cout<<"AUC[-3;0.5]:"<< singleTest(p.AUC(-3,0.5) == 0.5,res)<<std::endl;
        std::cout<<"AUC[0.25;0.5]:"<< singleTest(p.AUC(0.25,0.5) == 0.25,res)<<std::endl;
        std::cout<<"AUC[0.25;1.25]:"<< singleTest(p.AUC(0.25,1.25) == 1.25,res)<<std::endl;
        std::cout<<"AUC[1;1.5]:"<< singleTest(p.AUC(1,1.5) == 1,res)<<std::endl;
        std::cout<<std::endl;

        {
            std::cout<<"Test percentil_pointwise:"<<std::endl;
            Profil tmp=Profil({{0, Profil::Block(0,1,0.1)},
                               {1, Profil::Block(1,2,0.2)},
                               {3, Profil::Block(3,1,0.8)},
                               {4, Profil::Block(4,1,0.8)},
                               {5, Profil::Block(5,1,0.2)},
                               {6, Profil::Block(6,0.5,0.6)},
                               {6.5, Profil::Block(6.5,0,0.4)},
                           });
            std::cout<<"percentil_pointwise(2):"<< singleTest(tmp.percentil_pointwise(2) == 1,res)<<std::endl;
            std::cout<<"percentil_pointwise(-4):"<< singleTest(tmp.percentil_pointwise(-4) == 0,res)<<std::endl;
            std::cout<<"percentil_pointwise(0.2):"<< singleTest(std::abs(tmp.percentil_pointwise(0.2)-1/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_pointwise(0.5):"<< singleTest(std::abs(tmp.percentil_pointwise(0.5)-3.5/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_pointwise(0.8):"<< singleTest(std::abs(tmp.percentil_pointwise(0.8)-5.5/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_pointwise(2):"<< singleTest(tmp.percentil_pointwise(2) == 1,res)<<std::endl;


            std::cout<<"percentil_stepFunction(-4):"<< singleTest(tmp.percentil_stepFunction(-4) == 0,res)<<std::endl;
            std::cout<<"percentil_stepFunction(0.2):"<< singleTest(std::abs(tmp.percentil_stepFunction(0.2)-1/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_stepFunction(0.5):"<< singleTest(std::abs(tmp.percentil_stepFunction(0.5)-4/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_stepFunction(0.8):"<< singleTest(std::abs(tmp.percentil_stepFunction(0.8)-4.5/6.5)<0.00000001,res)<<std::endl;
            std::cout<<"percentil_stepFunction(0.8):"<< singleTest(std::abs(tmp.percentil_stepFunction(0.9)-1)<0.00000001,res)<<std::endl;
            std::cout<<std::endl;
        }
    }

    std::cout<<"---- End Profil -----"<<std::endl;
}
