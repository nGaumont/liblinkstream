#include "testMaster.hpp"
#include "node.hpp"
#include "link.hpp"
#include "group.hpp"
#include "partition.hpp"
#include "linkstream.hpp"
#include "profil.hpp"
#include "densityAnalyser.hpp"

using namespace linkstream;

bool TestMaster::all()const{
      std::pair<unsigned int, unsigned int> res;
      Node::test(res);
      Link::test(res);
      linkStream::test(res);
      Group::test(res);
      Partition::test(res);
      Profil::test(res);
      DensityAnalyser::test(res);
      std::cout<<"---- Final result -----"<<std::endl;
      std::cout<<res.second-res.first<<"/"<<res.second<<std::endl;
      return res.first==0;
}
