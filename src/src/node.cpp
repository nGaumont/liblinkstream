#include "node.hpp"
#include "link.hpp"
#include "tmpLink.hpp"

namespace linkstream{

void Node::push_new(const Link &l) {
    linkNeighbor.push_back(&l);
}
bool Node::pop_last(const Link &l){
    if (linkNeighbor.empty() || !l.weakEqual(*linkNeighbor[0])){
        return false;
    }
    linkNeighbor.pop_front();
    return true;
}
void Node::add(const Link &l){
    auto it = linkNeighbor.begin(),
         ite = linkNeighbor.end();

    auto tmp = lower_bound(it, ite, &l, linkComp);
    if(tmp==ite || (tmp!=ite && !(*tmp)->weakEqual(l)) ){
        linkNeighbor.insert(tmp,&l);
    }
}

bool Node::remove(const Link &l){
    auto it = linkNeighbor.begin(),
         ite = linkNeighbor.end();

    auto tmp = lower_bound(it, ite, &l, linkComp);
    if( tmp != it){
         --tmp;
    }
    if(tmp!=ite && (*tmp)->weakEqual(l)){
        linkNeighbor.erase(tmp);
        return true;
    }
    return false;
}



void Node::displayLinkNeighbor(std::string prefix) const {
  std::deque<const Link *>::const_iterator it = linkNeighbor.begin(),
                                     ite = linkNeighbor.end();
  std::cout << prefix << ": " << std::endl;
  for (; it != ite; ++it) {
    std::cout << "\t" << *(*it) << std::endl;
  }
}

bool Node::weakCompLinkNeighbor(const std::deque<tmpLink>& candidate){
    if (linkNeighbor.size() != candidate.size()){
        return false;
    }

    auto it2 = candidate.begin();
    bool result =true;

    for(const Link * l : linkNeighbor){
        if(!(*l).weakEqual(*it2)){
             result = false;
             std::cout<<"Diff "<< *l<<" "<< *it2<<std::endl;
        }
        ++it2;
    }
    return result;
}

bool Node::weakCompLinkNeighbor(const std::deque<const Link *>& candidate){
    if (linkNeighbor.size() != candidate.size()){
        std::cout<< "Pas la même taille"<<linkNeighbor.size()<<" "<<candidate.size()<<std::endl;
        return false;
    }

    auto it2 = candidate.begin();
    bool result =true;

    for(const Link * l : linkNeighbor){
        if(!(*l).weakEqual(**it2)){
             result = false;
             std::cout<<"Diff "<< *l<<" "<< **it2<<std::endl;
        }
        ++it2;
    }
    return result;
}

void Node::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<< "Node test:"<<std::endl;

    {
        Node n1(1), n2(2), n3(3);
        Link l1 (tmpLink(1.0,nodeID(1),nodeID(2), 1), n1, n2);
        Link l2 (tmpLink(0.5,nodeID(1),nodeID(3), 1), n1, n3);
        n1.push_new(l2);
        n1.push_new(l1);
        std::cout<<"push_new(l1);push_new(l2); :"<< singleTest(n1.weakCompLinkNeighbor({tmpLink(0.5,nodeID(1),nodeID(3),1),
                                                                              tmpLink(1,nodeID(1),nodeID(2),1)
                                                                             }), res) <<std::endl;
        std::cout<<"test_fail_pop :"<<singleTest(n1.pop_last(l1) == false,res)<<std::endl;
        std::cout<<"pop :"<<singleTest(n1.pop_last(l2),res)<<std::endl;
        std::cout<<"pop_last(l2) :"<< singleTest(n1.weakCompLinkNeighbor({tmpLink(1.0,nodeID(1),nodeID(2), 1)}), res) <<std::endl;
        std::cout<<"pop :"<<singleTest(n1.pop_last(l1),res)<<std::endl;
        std::cout<<"pop_last(l1) :"<< singleTest(n1.linksNeighbor().empty(), res) <<std::endl;
        std::cout<<"remove not present :"<<singleTest(n1.pop_last(l2) == false,res)<<std::endl;
    }

    {
        Node n1(1), n2(2), n3(3);
        Link l1 (tmpLink(1.0,nodeID(1),nodeID(2), 1), n1, n2);
        Link l2 (tmpLink(0.5,nodeID(1),nodeID(3), 1), n1, n3);
        n1.add(l1);
        n1.add(l2);
        std::cout<<"add(l1);add(l2); :"<< singleTest(n1.weakCompLinkNeighbor({tmpLink(0.5,nodeID(1),nodeID(3),1),
                                                                              tmpLink(1,nodeID(1),nodeID(2),1)
                                                                             }), res) <<std::endl;
        std::cout<<"remove(); :"<<singleTest(n1.remove(l2),res) <<std::endl;
        std::cout<<"test_remove(l2); :"<< singleTest(n1.weakCompLinkNeighbor({tmpLink(1,nodeID(1),nodeID(2),1)}), res) <<std::endl;
        std::cout<<"remove(l1); :"<<singleTest(n1.remove(l1),res) <<std::endl;
        std::cout<<"testremove(l1) :"<< singleTest(n1.linksNeighbor().empty(), res) <<std::endl;
        std::cout<<"remove not present :"<<singleTest(n1.remove(l2) == false,res) <<std::endl;
    }

    std::cout<< "End Node test: "<<res.second-res.first<<"/"<<res.second<<std::endl<<std::endl;

}


}
