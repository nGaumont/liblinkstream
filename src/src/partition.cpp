#include "partition.hpp"

using namespace linkstream;



Partition::Partition(const char *inputFile, const linkStream & L): com_struct(), assignation(){
    std::ifstream finput(inputFile);
    if (!finput) {
      std::cerr << "The file " << inputFile << " does not exist" << std::endl;
      exit(-1);
    }
    std::string line;
    linkID cur_link_id;
    groupID cur_com;
    while (std::getline(finput, line)) {
      std::istringstream iss(line);
      if (!(iss >> cur_link_id >> cur_com)) {
        std::cerr << "something wrong with:" << std::endl << line << std::endl;
        exit(-1);
      }

      const Link & cur_link = L.link(cur_link_id);

      // Check if cur_com already exist or not.
      std::pair<std::map<groupID, Group>::iterator ,bool> ins_group= com_struct.insert(std::pair<groupID, Group>(cur_com, linkstream::Group()));
      // true if a new element was inserted or false if an equivalent key already existed.
      Group & g= (*ins_group.first).second;
      if (ins_group.second){
          g.identifier=cur_com;
      }
      g.add_hint(g.link_set.end(), cur_link);

      std::pair<AssignationType::iterator ,bool> ins_link= assignation.insert(std::pair<const Link *, Group &>(&cur_link, g));
      if (!ins_link.second){
          std::cerr << "The link "<< cur_link<<" already has a group." << std::endl;
          throw LinkstreamException("link: Wrong linkId");
      }
    }
    finput.close();
}

Partition::Partition(const linkStream & L): com_struct(), assignation(){
    groupID gid=0;
    for(auto it = L.beginLink(); it !=L.endLink();++it){
        Group & g = com_struct[gid];
        g.identifier=gid;
        ++gid;
        g.add((*it)); // creation of the node
        std::pair<AssignationType::iterator ,bool> ins_link= assignation.insert(std::pair<const Link *, Group &>(&(*it), g));
        if (!ins_link.second){
            std::cerr << "The link "<< *it <<" already has a group." << std::endl;
            throw LinkstreamException("link: Wrong linkId");
        }
    }
}

bool Partition::checkCoverage(const linkStream & L)const{
    auto it = L.beginLink(), ite =L.endLink();
    // check that every link in L has a com assigned to it.
    for(; it != ite; ++it){
        if(assignation.find(&(*it)) ==  assignation.end()){
            return false;
        }
    }
    // every link in the cover is sur to be in L because of the constructor.
    return true;
}

Group & Partition::groupContaining(const Link & l){
    auto it = assignation.find(&l);
    if (it == assignation.end()) {
        std::cerr << "In the partition, there isn't any link with "<<l<<std::endl;
        exit(-1);
    }
    return (*it).second;
}



const Group & Partition::group(unsigned int gID)const{
    auto it = com_struct.find(gID);
    if (it == com_struct.end()) {
        std::cerr << "There isn't any group with id "<<gID<<std::endl;
        exit(-1);
    }
    return (*it).second;
}


const Group & Partition::containing(const Link & l)const{
    auto it = assignation.find(&l);
    if (it == assignation.end()) {
        std::cerr << "In the partition, there isn't any link with  "<<l<<std::endl;
        exit(-1);
    }
    return (*it).second;
}


unsigned int Partition::size()const{
    return com_struct.size();
}

//
// void Partition::moveInto(const Link & l, unsigned int gID){
//     Group &g = com_struct[gID];
//     Group &g2 = groupContaining(l);
//     g2.remove(l);
//     if(g2.size()==0){
//         com_struct.erase(g2.id());
//     }
//     g.add(l);
//     assignation[l.id] = g.id();
// }


void Partition::dumpAffiliation(std::ostream &out)const{
    for(AssignationType::const_iterator it = assignation.begin(); it!= assignation.end();++it){
        out<<*(*it).first<<" in "<<(*it).second<<std::endl;
    }
}


void Partition::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<<"---- Begin Partition -----"<<std::endl;
    linkStream L;
    L.readFile<TUVD>("test/test1.txt");
    Partition P("test/aff_test1.txt", L);
    std::set<nodeID> tmp;
    for( const Node * n : P.group(0).inducedNode()){
        tmp.insert((*n).id());
    }
    std::cout<<"inducedNode():"<< singleTest(tmp==std::set<nodeID>({nodeID(2),nodeID(3),nodeID(4)}),res)<<std::endl;
    std::cout<<"---- End Partition -----"<<std::endl;
}
