#include "densityAnalyser.hpp"

using namespace linkstream;

double DensityAnalyser::between(timestamp beg, timestamp end)const{
    long double acc = profil.AUC(beg, end);
    return linkstream::density(end-beg, acc, nodes.size());
}



Profil DensityAnalyser::variableStart(timestamp beg_min, timestamp beg_max, timestamp dur)const{
    assert(beg_max>beg_min);
    // If there is no intersection between [beg_min; beg_max+dur] and profil span then
    //Then there is only an empty profil
    if(beg_min >= profil.stop() || beg_max+dur <= profil.start()){
        return Profil({{beg_min, Profil::Block(beg_min,beg_max-beg_min,0)},
                       {beg_max, Profil::Block(beg_max,0,0)},
                              });
    }
    unsigned int nodeSize = nodes.size();
    Profil::const_iterator cur_beg=profil.at(beg_min, false),
                           cur_end=profil.at(beg_min+dur, false);
    long double cur_time= beg_min;
    //cur_beg should always contains cur_t
    //cur_end should always contains cur_t+dur;

    long double cum_deg = profil.AUC(cur_time, cur_time+dur);
    Profil p=Profil();

    // if [beg_min, begmin+dur] before the first block (cur_time+dur < (*cur_end).t )
    // or  begmin+dur is after the last possible block ending () AND beg-min is too early.
    if(cur_time+dur < (*cur_end).t  || ((*cur_end).stop() < cur_time+dur && (*cur_beg).start() > cur_time )){
        long double cur_dens = density(dur, cum_deg ,nodeSize);
        long double new_curtime;
        if((*cur_end).stop() > cur_time+dur){
            // advance so that cur_end is valid.
            new_curtime = (*cur_end).t-dur;
        }else{
            //cur_end is not a constraint beacause its ending is before the first ending
            // Therefore it should not be considered -> cur_end++ === profil.end().
            new_curtime = (*cur_beg).t;
            ++cur_end;
        }
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, new_curtime - cur_time, cur_dens));
        cur_time = new_curtime;
        // std::cout<<"Décalage "<< p <<std::endl;
        // std::cout<<"D "<< cur_time <<std::endl;
        // std::cout<<"D "<< (*cur_end) <<std::endl;
    }

    if( cur_end!=profil.end() && (*cur_end).stop() < cur_time+dur){
        //cur_end is not a constraint beacause its ending is before the first ending
        // Therefore it should not be considered -> cur_end++ === profil.end().
        ++cur_end;
    }

    // While there is still some time start to explore AND there still some block to process (either beg or end)
    while( cur_time < beg_max  && (cur_end!=profil.end() || cur_beg!=profil.end()) ){
        long double dist_1; // store the time to the next beg which can be
            //(*cur_beg).ending() if cur_time if in cur_beg
            //(*cur_beg).t if cur_time is before in cur_beg
            // Not existent if cur_beg already eached the end.
        if(cur_beg!=profil.end()){
            if((*cur_beg).t <= cur_time){
                dist_1 = std::min((*cur_beg).stop(), beg_max) - cur_time;
            }else{
                dist_1 = (*cur_beg).t - cur_time;
            }
        }else{
            dist_1 = -1;
        }
        long double dist_2= (cur_end!=profil.end() )?  std::min((*cur_end).stop(), beg_max+dur) - (cur_time+dur) : - 1; //time diff whith the next cur_end
        long double dist_min;
        // std::cout<<"B_max"<<beg_max<<std::endl;
        // std::cout<<"C "<<(*cur_beg)<<" "<< cur_time<<"  |  "<<(*cur_end) << " "<< (cur_time+dur)<<std::endl;
        // std::cout<<"C "<<dist_1<<"  |  "<<dist_2<<std::endl;
        if(dist_1>0 ){
            if(dist_2>0){
                // both distance are valid -> advance to the cloest one.
                dist_min = std::min(dist_1, dist_2);
                // std::cout<<"M "<<dist_1 <<" "<<dist_2 <<std::endl;
            }else{
                // only dist1 is valid -> advance to the dist1.
                dist_min = dist_1;
                // std::cout<<"M dist1"<<std::endl;
            }
        }else{
            // only dist2 is valid -> advance to the dist2.
            // dist_2 can't be negative because it would have been manage earlier.
            assert(dist_2>0);
            dist_min = dist_2;
            // std::cout<<"M dist2"<<std::endl;
        }

        // std::cout<<"A "<<cur_time<<" "<< dist_min <<" "<< cum_deg <<std::endl;

        long double cur_dens = density(dur, cum_deg ,nodeSize);
        // std::cout<<"Ajout ["<<cur_time<<","<< dist_min <<","<< cur_dens<<"]" <<std::endl;
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, dist_min, cur_dens));

        if(dist_1>0){
            if((*cur_beg).t <= cur_time){
                // Remove from deg if we actually remove deg from cur_beg
                // This happen if cur_beg is  before cur_time at the beginng.
                cum_deg -= dist_min*(*cur_beg).val;
                if(std::abs(dist_1-dist_min) < RESOLUTION){
                    cur_time = (*cur_beg).stop();
                    ++cur_beg;
                }
            }else{
                if(std::abs(dist_1-dist_min) < RESOLUTION){
                    cur_time = (*cur_beg).t;
                }
            }
        }
        if(dist_2>0){
            cum_deg += dist_min*(*cur_end).val;
        }

        if(dist_2>0 && std::abs(dist_2-dist_min) < RESOLUTION){
            //advance to he next end;
            cur_time = (*cur_end).stop()-dur;
            ++cur_end;
        }
        // std::cout<<"d "<<cum_deg<<std::endl;
        // std::cout<<"A "<<cur_time <<std::endl;
        // std::cout<<std::endl;
    }

    if(cur_time < beg_max){
        // empty block
        // std::cout<<"d "<<cum_deg<<std::endl;
        assert(cum_deg==0);
        p.profil.emplace_hint(p.profil.end(),cur_time, Profil::Block(cur_time, beg_max-cur_time, 0));
        cur_time = beg_max;
    }
    long double cur_dens = density(dur, cum_deg ,nodeSize);
    p.profil.emplace_hint(p.profil.end(),beg_max, Profil::Block(beg_max, 0, cur_dens));
    cur_time = (*cur_end).t-dur;
    return p;
}

Profil DensityAnalyser::variableDuration(timestamp dur_min, timestamp dur_max, timestamp beg)const{
    assert(dur_min<dur_max);
    timestamp end_min = beg+dur_min, end_max=beg+dur_max;
    Profil p=Profil();
    Profil::const_iterator cur=profil.at(beg+dur_min, false),ite= profil.end();
    double cum_deg = profil.AUC(beg, end_min);
    unsigned int nodeSize = nodes.size();
    long double cur_dur= dur_min;

    //handle if beg is before the first block
    if((*cur).start() > end_min){
        // there is no block between end_min and cur.t
        long double span_used= (*cur).t-end_min;
        p.profil.emplace_hint(p.profil.end(),dur_min, Profil::Block(dur_min, span_used, 0));
        cur_dur = (*cur).t-beg;
    }

    //handle when beg is in the middle of the block
    if((*cur).start() < end_min){
        double leng_of_dur = std::min((*cur).stop(),end_max) - end_min;
        double cur_dens = density(dur_min,cum_deg ,nodeSize);

        p.profil.emplace_hint(p.profil.end(),dur_min, Profil::Block(dur_min, leng_of_dur, cur_dens));
        cum_deg += leng_of_dur * (*cur).val;
        cur_dur += leng_of_dur;
        ++cur;
    }

    while(cur != ite && (*cur).stop()<=end_max){
        double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, (*cur).d, cur_dens));
        cum_deg += (*cur).d* (*cur).val;
        cur_dur = (*cur).stop()-beg;
        ++cur;
    }
    //Handle the last block
    if(cur != ite && cur_dur+beg <= (*cur).stop() + RESOLUTION && (*cur).t < end_max ){
        long double leng_of_dur = end_max - (*cur).t;
        long double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, leng_of_dur, cur_dens));
        cum_deg += leng_of_dur * (*cur).val;
        cur_dur += leng_of_dur;
    }

    if(cur_dur<dur_max){
        long double cur_dens = density(cur_dur,cum_deg ,nodeSize);
        p.profil.emplace_hint(p.profil.end(),cur_dur, Profil::Block(cur_dur, dur_max-cur_dur, cur_dens));
        cur_dur = dur_max;
    }

    //Compute the density for the last value.
    long double cur_dens = density(dur_max,cum_deg ,nodeSize);
    p.profil.emplace_hint(p.profil.end(),dur_max, Profil::Block(dur_max, 0, cur_dens));
    return p;
}


void DensityAnalyser::test(std::pair<unsigned int, unsigned int> & res){
    std::cout<<"---- Begin DensityAnalyser -----"<<std::endl;

    linkStream L;
    L.readFile<TUVD>("test/test1.txt");
    std::cout<<std::setprecision(16);
    std::set<nodeID> nodes;

    for(auto it = L.beginNode(); it !=L.endNode();++it){
        nodes.insert((*it).id());
    }
    DensityAnalyser d(L,nodes, true, true);

    std::cout<<"---- Profil -----"<<std::endl;
    {
        Profil tmp =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,2)},
                              {1.5, Profil::Block(1.5,0.5,1.0)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,1,1)},
                              {5, Profil::Block(5,0.5,2)},
                              {5.5, Profil::Block(5.5,0.5,1)},
                              {6, Profil::Block(6,1,2)},
                          });
        std::cout<<"Construction:"<<singleTest(d.profil == tmp,res)<<std::endl;
        std::cout<<"isconsistent:"<<singleTest(d.profil.isconsistent(),res)<<std::endl;
        DensityAnalyser d2(L,true);

        std::cout<<"Construction2:"<<singleTest(d.profil == d2.profil ,res)<<std::endl;
        DensityAnalyser d3(L,nodes, true, false);
        std::cout<<"Construction3:"<<singleTest(d.profil == d3.profil ,res)<<std::endl;
        // std::cout<<d.profil<<std::endl<<tmp<<std::endl;
    }
    std::cout<<std::endl;


    {
        Profil tmp2 =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.5,4)},
                              {1.5, Profil::Block(1.5,0.5,3)},
                              {2, Profil::Block(2,1,4)},
                              {3, Profil::Block(3,1,2)},
                              {4, Profil::Block(4,0.5,1)},
                              {4.5, Profil::Block(4.5,0.2,2)},
                              {4.7, Profil::Block(4.7,0.3,1)},
                              {5, Profil::Block(5,0.25,2)},
                              {5.25, Profil::Block(5.25,0.25,3)},
                              {5.5, Profil::Block(5.5,0.5,2)},
                              {6, Profil::Block(6,0.25,3)},
                              {6.25, Profil::Block(6.25,0.75,2)},
                              });
        Profil p_(d.profil);
        Profil::iterator it = p_.at(1.2, false);
        p_.modifier_hint(it, Profil::Block(1,3,2.0),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        p_.modifier_hint(p_.at(4.5, false), Profil::Block(4.5,0.2,1),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        p_.modifier_hint(p_.at(5.25, false), Profil::Block(5.25,1,1.0),[](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        std::cout<<"modifier_hint():"<< singleTest(p_==tmp2,res)<<std::endl;
    }

    {
        std::cout<<"Test add:"<<std::endl;
        Profil p_res =  Profil({{0, Profil::Block(0,1.0,1.0)},
                               {1, Profil::Block(1,0.25,2)},
                               {1.25, Profil::Block(1.25,0.1,3)},
                               {1.35, Profil::Block(1.35,0.15,2)},
                               {1.5, Profil::Block(1.5,0.5,1)},
                               {2, Profil::Block(2,1,2)},
                               {3, Profil::Block(3,1,0)},
                               {4, Profil::Block(4,0.5,1)},
                               {4.5, Profil::Block(4.5,0.5,2)},
                               {5, Profil::Block(5,0.5,3)},
                               {5.5, Profil::Block(5.5,0.5,2)},
                               {6, Profil::Block(6,0.5,3)},
                               {6.5, Profil::Block(6.5,0.5,2)},
                              });
        Profil tmp =   Profil({{1.25, Profil::Block(1.25,0.1,1.0)},
                               {4.5, Profil::Block(4.5,2,1)},
                              });
        Profil p_(d.profil);

        p_.add(tmp);
        std::cout<<"add():"<< singleTest(p_==p_res,res)<<std::endl;
        Profil p_2(d.profil);
        p_2.modifier(tmp, [](const Profil::Block & x, const Profil::Block & y) {return x.val+y.val;});
        std::cout<<"modifier(, [](){+}):"<< singleTest(p_2==p_res,res)<<std::endl;
    }
    {
      Profil p_res =  Profil({{0, Profil::Block(0,1.0,1.0)},
                              {1, Profil::Block(1,0.25,2)},
                              {1.25, Profil::Block(1.25,0.1,1)},
                              {1.35, Profil::Block(1.35,0.15,2)},
                              {1.5, Profil::Block(1.5,0.5,1)},
                              {2, Profil::Block(2,1,2)},
                              {3, Profil::Block(3,1,0)},
                              {4, Profil::Block(4,0.5,1)},
                              {4.5, Profil::Block(4.5,0.5,0.5)},
                              {5, Profil::Block(5,0.5,1)},
                              {5.5, Profil::Block(5.5,0.5,0.5)},
                              {6, Profil::Block(6,0.5,1)},
                              {6.5, Profil::Block(6.5,0.5,2)},
                             });
      Profil tmp =   Profil({{1.25, Profil::Block(1.25,0.1,2)},
                              {4.5, Profil::Block(4.5,2,2)},
                             });
      Profil p_(d.profil);
      p_.modifier(tmp, [](const Profil::Block & x, const Profil::Block & y) {return x.val/y.val;});
      std::cout<<"modifier(, [](){/}):"<< singleTest(p_==p_res,res)<<std::endl;
    }

    {

        Partition C("test/aff_test1.txt", L);
        Profil p(C.group(0));
        Profil p2(-5,15,3);
        p.modifier(p2, [](const Profil::Block & x, const Profil::Block & y) {return x.val/y.val;});
        Profil p_res =  Profil({{4, Profil::Block(4,1,1.0/3.0)},
                              {5, Profil::Block(5,0.5,2.0/3.0)},
                              {5.5, Profil::Block(5.5,0.5,1.0/3.0)},
                              {6, Profil::Block(6,1,2.0/3.0)},
                          });
        std::cout<<"modifier(, [](){/})_bis:"<< singleTest(p==p_res,res)<<std::endl;
    }

    std::cout<<"Test between:"<<std::endl;
    std::cout<<"between[0;7]:"<< singleTest(d.between(0,7) == density(7,9,4),res)<<std::endl;
    std::cout<<"between[-3;42]:"<< singleTest(d.between(-3,42) == density(45,9,4),res)<<std::endl;
    std::cout<<"between[-3;0.5]:"<< singleTest(d.between(-3,0.5) == density(3.5,0.5,4),res)<<std::endl;
    std::cout<<"between[0.25;0.5]:"<< singleTest(d.between(0.25,0.5) == density(0.25,0.25,4),res)<<std::endl;
    std::cout<<"between[0.25;1.25]:"<< singleTest(d.between(0.25,1.25) == density(1,1.25,4),res)<<std::endl;
    std::cout<<"between[1;1.5]:"<< singleTest(d.between(1,1.5) == density(0.5,1,4),res)<<std::endl;
    std::cout<<"between[10;12]:"<< singleTest(d.between(10,12) == density(2,0,4),res)<<std::endl;
    std::cout<<std::endl;

    // DensityAnalyser(const linkStream & L_, const Group &g1, const Group &g2);
    {
        std::cout<<"Test variableDuration:"<<std::endl;
        Profil p_res =  Profil({{2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(2,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(2,5.5))},
                               {4, Profil::Block(4,1,d.between(2,6))},
                               {5, Profil::Block(5,0,d.between(2,7))},
                              });
        Profil tmp = d.variableDuration(2,5,2);
        std::cout<<"variableDuration(2,5,2):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{2, Profil::Block(2,0.5,d.between(2.5,4.5))},
                               {2.5, Profil::Block(2.5,0.5,d.between(2.5,5))},
                               {3, Profil::Block(3,0.5,d.between(2.5,5.5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(2.5,6))},
                               {4, Profil::Block(4,0,d.between(2.5,6.5))},
                              });
        tmp = d.variableDuration(2, 4, 2.5);
        std::cout<<"variableDuration(2,4,2.5):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{2, Profil::Block(2,1,d.between(-3,-1))},
                               {3, Profil::Block(3,1,d.between(-3,0))},
                               {4, Profil::Block(4,0.5,d.between(-3,1))},
                               {4.5, Profil::Block(4.5,0.5,d.between(-3,1.5))},
                               {5, Profil::Block(5,0,d.between(-3,2))},
                              });
        tmp = d.variableDuration(2, 5, -3);
        std::cout<<"variableDuration(2,5,-3):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{2, Profil::Block(2,1,d.between(2,4))},
                       {3, Profil::Block(3,0.5,d.between(2,5))},
                       {3.5, Profil::Block(3.5,0.5,d.between(2,5.5))},
                       {4, Profil::Block(4,1,d.between(2,6))},
                       {5, Profil::Block(5,45,d.between(2,7))},
                       {50, Profil::Block(50,0,d.between(2,52))},
                      });
        tmp = d.variableDuration(2, 50, 2);
        std::cout<<"variableDuration(2,50,2):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{0.1, Profil::Block(0.1,0.5,d.between(2.1,2.2))},
                       {0.6, Profil::Block(0.6,0,d.between(2.1,2.7))},
                      });
        tmp = d.variableDuration(0.1, 0.6, 2.1);
        std::cout<<"variableDuration(0.1,0.6,2.1):"<< singleTest(tmp==p_res,res)<<std::endl;
        std::cout<<std::endl;
    }
    {
        std::cout<<"Test variableStart:"<<std::endl;
        Profil p_res =  Profil({{1.25, Profil::Block(1.25,0.25,d.between(1.25,3.25))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.3,d.between(3.5,5.5))},
                               {3.8, Profil::Block(3.8,0,d.between(3.8,5.8))},
                              });
        auto tmp = d.variableStart(1.25, 3.8, 2);
        std::cout<<"variableStart(1.25,3.8,2):"<< singleTest(tmp==p_res,res)<<std::endl;

        std::cout<<std::endl;
        Profil res2 =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.3,d.between(3.5,5.5))},
                               {3.8, Profil::Block(3.8,0,d.between(3.8,5.8))},
                              });
        tmp = d.variableStart(-3, 3.8, 2);
        std::cout<<"variableStart(-3,3.8,2):"<< singleTest(tmp==res2,res)<<std::endl;


        p_res =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(3.5,5.5))},
                               {4, Profil::Block(4,1,d.between(4,6))},
                               {5, Profil::Block(5,0.5,d.between(5,7))},
                               {5.5, Profil::Block(5.5,0,d.between(5.5,7.5))},
                              });
        tmp = d.variableStart(-3, 5.5, 2);
        std::cout<<"variableStart(-3,5.5,2):"<< singleTest(tmp==p_res,res)<<std::endl;


        p_res =  Profil({{-3, Profil::Block(-3,1,d.between(-3,-1))},
                               {-2, Profil::Block(-2,1,d.between(-2,0))},
                               {-1, Profil::Block(-1,0.5,d.between(-1,1))},
                               {-0.5, Profil::Block(-0.5,0.5,d.between(-0.5,1.5))},
                               {0, Profil::Block(0,1,d.between(0,2))},
                               {1, Profil::Block(1,0.5,d.between(1,3))},
                               {1.5, Profil::Block(1.5,0.5,d.between(1.5,3.5))},
                               {2, Profil::Block(2,1,d.between(2,4))},
                               {3, Profil::Block(3,0.5,d.between(3,5))},
                               {3.5, Profil::Block(3.5,0.5,d.between(3.5,5.5))},
                               {4, Profil::Block(4,1,d.between(4,6))},
                               {5, Profil::Block(5,0.5,d.between(5,7))},
                               {5.5, Profil::Block(5.5,0.5,d.between(5.5,7.5))},
                               {6, Profil::Block(6,1,d.between(6,8))},
                               {7, Profil::Block(7,3,d.between(7,9))},
                               {10, Profil::Block(10,0,d.between(10,12))},
                              });
        tmp = d.variableStart(-3,10,2);
        std::cout<<"variableStart(-3,10,2):"<< singleTest(tmp==p_res,res)<<std::endl;


        p_res =  Profil({{-1, Profil::Block(-1,1,d.between(-1,11))},
                        {0, Profil::Block(0,1,d.between(0,12))},
                        {1, Profil::Block(1,0.5,d.between(1,13))},
                        {1.5, Profil::Block(1.5,0.5,d.between(1.5,13.5))},
                        {2, Profil::Block(2,1,d.between(2,14))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(-1,10,12);
        std::cout<<"variableStart(-1,10,12):"<< singleTest(tmp==p_res,res)<<std::endl;


        p_res =  Profil({{2.5, Profil::Block(2.5,0.5,d.between(2.5,14.5))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(2.5,10,12);
        std::cout<<"variableStart(2.5,10,12):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{2, Profil::Block(2,1,d.between(2,14))},
                        {3, Profil::Block(3,1,d.between(3,15))},
                        {4, Profil::Block(4,1,d.between(4,16))},
                        {5, Profil::Block(5,0.5,d.between(5,17))},
                        {5.5, Profil::Block(5.5,0.5,d.between(5.5,17.5))},
                        {6, Profil::Block(6,1,d.between(6,18))},
                        {7, Profil::Block(7,3,d.between(7,19))},
                        {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(2,10,12);
        std::cout<<"variableStart(2,10,12):"<< singleTest(tmp==p_res,res)<<std::endl;

        p_res =  Profil({{7, Profil::Block(7,3,d.between(7,19))},
                       {10, Profil::Block(10,0,d.between(10,22))},
                              });
        tmp = d.variableStart(7,10,12);
        std::cout<<"variableStart(7,10,12):"<< singleTest(tmp==p_res,res)<<std::endl;
        std::cout<<std::endl;
    }
    {
        Profil prof =  Profil({{1353303380, Profil::Block(1353303380,20,1)},
                               {1353303400, Profil::Block(1353303400,880,1)},
                               {1353304280, Profil::Block(1353304280,60,1)},
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil p_res =  Profil({{1353303360, Profil::Block(1353303360,20,920.0/940.0)},
                               {1353303380, Profil::Block(1353303380,20,1.0)},
                               {1353303400, Profil::Block(1353303400,880,1.0)},
                               {1353304280, Profil::Block(1353304280,60,60.0/940)},
                               {1353304340, Profil::Block(1353304340,728540,0)},
                               {1354032880, Profil::Block(1354032880,0,0)},
                              });
        Profil tmp = anlyser.variableStart(1353303360, 1354032880, 940);
        std::cout<<"variableStart(1353303360,1354032880,940):"<< singleTest(tmp==p_res,res)<<std::endl;
    }

    {
        Profil prof =  Profil({{1353304000, Profil::Block(1353304000,20,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil p_res =  Profil({{1353303360, Profil::Block(1353303360,620,0)},
                               {1353303980, Profil::Block(1353303980,20,0)},
                               {1353304000, Profil::Block(1353304000,20,1)},
                               {1353304020, Profil::Block(1353304020,728840,0)},
                               {1354032860, Profil::Block(1354032860,0,0)},
                              });
        Profil tmp = anlyser.variableStart(1353303360, 1354032860, 20);
        std::cout<<"variableStart(1353303360,1354032860,20):"<< singleTest(tmp==p_res,res)<<std::endl;
    }
    {
        Profil prof =  Profil({{1353304000, Profil::Block(1353304000,20,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        Profil p_res =  Profil({{1353304005, Profil::Block(1353304005,5,0.5)},
                               {1353304010, Profil::Block(1353304010,0,1.0/3.0)},
                              });
        Profil tmp = anlyser.variableStart(1353304005, 1353304010, 30);
        std::cout<<"variableStart(1353304005,1353304010,30):"<< singleTest(tmp==p_res,res)<<std::endl;
    }
    {
        Profil prof =  Profil({{50, Profil::Block(50,1,1)}
                              });
        DensityAnalyser anlyser(prof, {1,2});
        std::cout<<"Test variableDuration:"<<std::endl;
        Profil p_res =  Profil({{2, Profil::Block(2,8,0)},
                               {10, Profil::Block(10,1,0)},
                               {11, Profil::Block(11,9,1.0/11.0)},
                               {20, Profil::Block(20,0,1.0/20.0)},
                              });
        Profil tmp = anlyser.variableDuration(2,20,40);
        std::cout<<"variableDuration(2,5,2):"<< singleTest(tmp==p_res,res)<<std::endl;
    }


    std::cout<<"---- End DensityAnalyser -----"<<std::endl;
}
