#include "link.hpp"

namespace linkstream{

void Link::linkNeighbor(std::deque<const Link *> &res) const {
  res.clear();
  const std::deque<const Link *> &left = this->left().linksNeighbor();
  const std::deque<const Link *> &right = this->right().linksNeighbor();
  res.resize(left.size() +
             right.size()); // make sur res is large enough to store everything.

  std::deque<const Link *>::iterator ite;
  ite = std::set_union(left.begin(), left.end(), right.begin(), right.end(),
                       res.begin(), linkComp); // fill res with the union
  // ite points to the end of added values
  res.resize(ite - res.begin()); // As doublon could occurs=> ite!= res.end().
                                 // Therefore res has to be resized.
                                 // The current link is in the dequeu;
}

std::pair<bool, nodeID> Link::commonNode(const Link &l) const {
  std::pair<bool, nodeID> res(false, nodeID(0));
  if (left().id() == l.left().id() || left().id() == l.right().id()) {
    res.first = true;
    res.second = left().id();
  } else if (right().id() == l.left().id() || right().id() == l.right().id()) {
    res.first = true;
    res.second = right().id();
  }
  return res;
}

bool Link::weakCompLinkNeighbor(const std::deque<tmpLink>& candidate)const{
     std::deque<const Link *> neighbor;
     linkNeighbor(neighbor);
    if (neighbor.size() != candidate.size()){
        return false;
    }

    auto it2 = candidate.begin();
    bool result =true;

    for(const Link * l : neighbor){
        if(!(*l).weakEqual(*it2)){
             result = false;
             std::cout<<"Diff "<< *l<<" "<< *it2<<std::endl;
        }
        ++it2;
    }
    return result;
}

bool Link::weakCompLinkNeighbor(const std::deque<const Link *>& candidate)const{
     std::deque<const Link *> neighbor;
     linkNeighbor(neighbor);
    if (neighbor.size() != candidate.size()){
        return false;
    }

    auto it2 = candidate.begin();
    bool result =true;

    for(const Link * l : neighbor){
        if(!(*l).weakEqual(**it2)){
             result = false;
             std::cout<<"Diff "<< *l<<" "<< **it2<<std::endl;
        }
        ++it2;
    }
    return result;
}

void Link::test(std::pair<unsigned int, unsigned int> &){
    std::cout<< "Link test:"<<std::endl;
    std::cout<< "End Link test:"<<std::endl;
}

}
