import Linkstream as ls


print("Begin test:")
if(ls.test()):
    print("Everything is fine on the C++ part.")
else:
    print("There are already some problem on the C++ part.")
    exit(-1)

L = ls.Linkstream()
L.readFileTUV("test/test1_TUV.txt",2)
assert(L.isSimple()==True)
assert(str([l for l in L.links()])=='[-1 1 2 2, 0 2 3 2, 1 2 1 2, 3 2 3 2, 4 3 4 2, 5 2 3 2]')
assert(str(L.link(0))== '-1 1 2 2')
assert(str(L.node(1))== '1')
assert(str([n for n in L.nodes()])=='[1, 2, 3, 4]')
assert(str([l for l in L.node(2).links()])=='[-1 1 2 2, 0 2 3 2, 1 2 1 2, 3 2 3 2, 5 2 3 2]')


assert(str([l for l in L.edge(1,2)]) == '[-1 1 2 2, 1 2 1 2]')
assert([e.edge() for e in L.edges()] == [(1, 2), (2, 3), (3, 4)])
assert(str([l for e in L.edges() for l in e ]) == '[-1 1 2 2, 1 2 1 2, 0 2 3 2, 3 2 3 2, 5 2 3 2, 4 3 4 2]')
tmplink = ls.tmpLink(0.0, 2, 3, 2.0)
L.removeLink(tmplink)
assert(str([l for l in L.links()])=='[-1 1 2 2, 1 2 1 2, 3 2 3 2, 4 3 4 2, 5 2 3 2]')
L.addLink(tmplink)
assert(str([l for l in L.links()])=='[-1 1 2 2, 0 2 3 2, 1 2 1 2, 3 2 3 2, 4 3 4 2, 5 2 3 2]')


L = ls.Linkstream()
L.readFileTUVD("test/test1.txt")
Partition = ls.Partition("test/aff_test1.txt",L)
assert(Partition.checkCoverage(L))
assert([g.id() for g in Partition.groups()]== [0, 1])
assert(str([l for l in Partition.group(1).links()])== '[0 1 2 1.5, 1 2 3 2, 2 1 2 1]')
assert(str([n for n in Partition.group(1).inducedNodes()]) == '[1, 2, 3]')

P = ls.Profil(L, True)
P2 = ls.Profil(Partition.group(1) , True)
P3 = ls.Profil(P)

assert(str([b for b in P.blocks()])=="[[0, 1, 1], [1, 0.5, 2], [1.5, 0.5, 1], [2, 1, 2], [3, 1, 0], [4, 1, 1], [5, 0.5, 2], [5.5, 0.5, 1], [6, 1, 2]]")
assert(P.size()==9)
assert(P.AUC(0,7)==9)
assert(P.AUC(0.25,1.25)==1.25)
assert(abs(P.percentil_pointwise(4)-1) < 0.0001)
assert(str(P.at(1, True))== "[0, 1, 1]")
assert(str(P.at(1, False))== "[1, 0.5, 2]")
assert(str(P.maxVal())== "[1, 0.5, 2]")
assert(P==P3)

D= ls.DensityAnalyser(L,True)
assert(D.between(0,7)==1.0/6.0*9.0/7.0)
D.variableStart(1.25, 3.8,2)
D.variableStart(2,5,2)

D2= ls.DensityAnalyser(Partition.group(1), True)
assert(D2.between(0,3)==1.0/3.0*4.5/3.0)

D3 = ls.DensityAnalyser(L,[1,2,3,4], False, False)
assert(D.between(0,7)==D3.between(0,7))


print("Everything works fine.")
