Linkstream
==========

This library intends to work like [networkx](https://networkx.github.io/) but for link streams.  
It is written in C++ with a binding to python thanks to [pybind11](https://github.com/pybind/pybind11).

Installation
-----------


If you want the latest version::
```
git clone https://nGaumont@bitbucket.org/nGaumont/liblinkstream.git
pip install ./liblinkstream
```

To make sure everything works:  
```
cd test
python run_test.py
```

Documentation  
--------------

The documentation is hosted [here](https://linkstream.ngaumont.fr/).  
You can also build it yourself. 
It is generated using Sphinx:  
```
cd liblinkstream/docs
make html
```

The built website is in ``liblinkstream/docs/_build/html``.