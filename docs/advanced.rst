.. _advanced:


Advanced Topic
##############


Build C++ library
=================


If you want to build the C++ library without any python binding.
You should use the following commands::

  cd Linkstream/build
  cmake ..
  make

This should generate the file ``lib/libLinkstream.so`` which is a dynamic library.
To link against this library, please see the example :file:`example/C++_Example/`
