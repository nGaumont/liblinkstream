.. _Partition:
.. currentmodule:: Linkstream

Partition
=========

.. autoclass:: Partition
    :members:
