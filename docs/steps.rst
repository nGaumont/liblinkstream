.. _steps:


First example
#############

Hello world
===========

Here is a small example reading a link stream from a file::

  import Linkstream as ls

  L = ls.Linkstream()
  L.readFileBEUV("SomeFile")
  print("This streams contains", L.sizeLink(),"links between", L.sizeNode())
  print("Link list:")
  for l in L.links():
    print(l)

It's also possible to add and remove link manually::

  import Linkstream as ls

  L = ls.Linkstream()
  L.addLink(ls.tmpLink(t1,u,v,duration))
  # t1 is a time, u and v are nodeID and duration is a time duration.
  print("Link list:")
  for l in L.links():
    print(l)


Draw link streams
=================

It's also possible to plot a linkstream::

  import Linkstream as ls
  import PlotLinkstream as plt

  L = ls.Linkstream()
  L.readFileTUVD("test/test/test1.txt")
  P = ls.Partition("test/test/aff_test1.txt", L) # It's a link partition
  plt.draw(L, "Outputfile.svg", P) # P is used for colors and is optional


Density in link streams
=======================

It's easy to compute the density on a given interval of a whole link stream, a group of linksor a group of nodes::

  import Linkstream as ls

  L = ls.Linkstream()
  L.readFileTUVD("test/test/test1.txt")
  P = ls.Partition("test/test/aff_test1.txt", L) # It's a link partition
  D = ls.DensityAnalyser(L,True) # True stand for simple link stream
  print(D.between(L.start(),L.stop()))  # density over the whole link stream
  print(D.between(1,3))  # density of the whole link stream in [1,3]

  g = ls.Partition.group(1)
  D2 = ls.DensityAnalyser(g, True)
  D2.between(g.start(), g.stop())  # density of the group of links


  D3 = ls.DensityAnalyser(L,[1,2,4], False, False)
  # First False means all the links having at least one end in [1,2,4] are considered
  # If it is false, only links in [1,2,4] x [1,2,4] are considered.
  # Second False means that the links considered may not be simple.

  D3.between(4, 7)  # density of a given set of nodes in [4,7]



Evaluate groups relevancy in link streams
==========================================

Here is an example on how to compute the relevancy of a group of links::

  import Linkstream as ls

  L = ls.Linkstream()
  isSimple= L.isSimple()
  L.readFileTUVD("test/test/test1.txt")
  P = ls.Partition("test/test/aff_test1.txt", L)
  D = ls.DensityAnalyser(g, isSimple)

  print("Nodes aspect:",D.variableNodes(L, g.start(), g.stop()))

  # For the start time and duration aspects, it's not so easy
  # We first have to compute the function start time to density and duration to density.
  startToDensity = D.variableStart(L.start(), L.stop()-g.duration(), g.duration())
  g_dens = D.between(g.start(),g.stop())
  print("Start time aspect:", startToDensity.percentil_pointwise(g_dens))

  durationToDensity = D.variableDuration(1, L.duration(), g.start())
  print("Duration aspect:",durationToDensity.percentil_pointwise(g_dens))



File format
===========


This library reads link streams in three formats:
 * ``BEUV`` stands for Begin, End U ,V.
 * ``TUVD`` stands for Times, U, V, Duration.
 * ``TUV`` stands for Times, U, V.


+-----------------+--------------+-----------+
|      BEUV       |    TUVD      |    TUV    |
+=================+==============+===========+
|   t_1 t_35 u v  |  t_1 u v 34  |  t_1 u v  |
+-----------------+--------------+-----------+
|   t_2 t_3 v w   |  t_2 v w 1   |  t_2 v w  |
+-----------------+--------------+-----------+
