.. _basics:

Installation and documentation
##############################

Installation
============


If you want the latest version::

  git clone https://nGaumont@bitbucket.org/nGaumont/liblinkstream.git
  pip install ./liblinkstream



To make sure everything works::

  cd test
  python run_test.py


Requirements
============

 * python-dev or python-devel
 * gcc capable of compiling C++11 

Building the documentation
==========================

Documentation for the library is generated using Sphinx.
To generate it locally::

  cd Linkstream/docs
  make html


The built website is in ``liblinkstream/docs/_build/html``.
